CC=g++ -Warray-bounds -Wdeprecated -Wempty-body -Wlogical-op -Wmissing-braces -Wmissing-include-dirs -Woverflow -Wparentheses -Wreturn-type -Wunused-value -Wunused-variable
# CC=icpc -gcc-name=gcc-4.3 -gxx-name=g++-4.3 -Wno-unknown-pragmas
SRC =$(wildcard *.cpp */*.cpp)
OBJ=$(SRC:.cpp=.o)
EXECUTABLE=segment

# MKL includes/libraries.
MKLPATH=$(MKLROOT)/lib/em64t
CMKL= -I$(MKLROOT)/include -DUSE_MKL
LMKL= -L$(MKLPATH) -lmkl_solver_lp64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm

LDFLAGS=$(LMKL)
CFLAGS=-c $(CMKL) -std=c++0x

all: CFLAGS+=-O3 -DINDEX_CHECK
all: $(SRC) $(EXECUTABLE) tags

debug: CFLAGS+=-g -ggdb -DINDEX_CHECK
debug: LDFLAGS+=-g
debug: $(SRC) $(EXECUTABLE) tags

profile: CFLAGS+=-c -O2 -pg
profile: LFLAGS+=-pg
profile: $(SRC) $(EXECUTABLE) tags

$(EXECUTABLE): $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

.PHONY : tags
tags:
	@ctags -R --c++-kinds=+plx --fields=+fKstiaSm --extra=+q --language-force=C++ .

clean:
	@rm -f $(OBJ) tags

#include "LevelSet_PDE.h"
#ifdef _MSC_VER
#include <float.h>
#define isfinite(x) (_finite(x))
#endif

namespace matcad
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the initial level set.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::initialize(Vector& (*ls)(Vector&,double), double s_min, double s_max, unsigned pts) {
        const double ds = (s_max-s_min)/pts;
        _ls_eqn = ls;

        MinLSpts min_ls_pts(_ls.size, _NDIM);
        _calc_distances(ds, s_min, pts, min_ls_pts);
        _calc_signs(ds, min_ls_pts);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Extend the velocity across the whole domain.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::velocity_extension() {
        //TODO: this initializes the vel vector. Should be done in another way or somewhere else.
        Vector row;
        for(unsigned i=0; i<_ls.size; i++){
            vectorU32 &neighbors = _conn[i];
            for(unsigned j=0; j<neighbors.size(); j++){
                if(_ls.phi(i)*_ls.phi(neighbors[j]) <= 0.0){
                    // _ls.vel(i) = _F(_ls.levelset.row(i));  // Ideally use this.
                    row = _grid.row(i);
                    _ls.vel(i) = _F(row);
                    break;
                }
            }
        }

        double dtau(0.005), H_max;
        Vector vel(_ls.vel), S(_ls.size);
        // Approximated value of the sign of phi.
        //TODO: we're using S in this function and in reinit...store in the SDF struct?
        double h = _grid(0,1) - _grid(1,1);    // TODO: this should be the current grid spacing.
        for(unsigned i=0; i<_ls.size; i++) S(i) = _S(_ls.phi(i),h);

        size_t itr = 0;
        do{
            H_max = 0.0;
            for(unsigned i=0; i<_ls.size; i++){
                // Detect location of the interface by a sign difference.
                vectorU32 &neighbors = _conn[i];
                bool local = false;
                for(unsigned j=0; j<neighbors.size(); j++){
                    if(_ls.phi(i)*_ls.phi(neighbors[j]) <= 0.0){
                        local = true;
                        break;
                    }
                }
                if(!local){
                    Vector F_d   = _hamiltonian_derivatives(_ls.vel, neighbors, i);
                    Vector phi_d = _hamiltonian_derivatives(_ls.phi, neighbors, i);

                    Vector n(_NDIM, true);
                    for(unsigned j=0; j<_NDIM; j++){
                        if(phi_d(2*j) != 0.0)        n(j) = phi_d(2*j);
                        else if(phi_d(2*j+1) != 0.0) n(j) = phi_d(2*j+1);
                    }
                    double normal = norm(n);
                    if(isfinite(normal)) n *= S(i)/normal;
                    double H = max(n(0),0.0)*F_d(0) + min(n(0),0.0)*F_d(1) +
                               max(n(1),0.0)*F_d(2) + min(n(1),0.0)*F_d(3);
                    vel(i) -= dtau * H;
                    if(fabs(H) > H_max) H_max = fabs(H);
                }
            }
            _ls.vel = vel;
            itr++;
        }while(fabs(H_max) > 1e-10 && itr < 1000);
        std::cout<<"Velocity Extension iters:"<<itr<<", H_max:"<<H_max<<"\n";
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Reinitialize the SDF.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::reinitialize() {
        //TODO: There's a bug in here, doesn't recalculate points right next to interface,
        // so if your interface is way off, this won't fix it.
        double dtau(0.005), H_max;
        Vector phi_0(_ls.phi), phi_temp(_ls.phi), S(_ls.size);
        // Approximated value of the sign of phi.
        double h = _grid(0,1) - _grid(1,1);    // TODO: this should be the current grid spacing.
        for(unsigned i=0; i<_ls.size; i++) S(i) = _S(_ls.phi(i),h);

        size_t itr = 0;
        do{
            H_max = 0.0;
            for(unsigned i=0; i<_ls.size; i++){
                // If the displacement lies between this node and a neighbor, use the sub-cell fix.
                // Detect location of the interface by a sign difference.
                vectorU32 &neighbors = _conn[i];
                bool local = false;
                for(unsigned j=0; j<neighbors.size(); j++){
                    if(phi_0(i)*phi_0(neighbors[j]) <= 0.0){
                        phi_temp(i) -= dtau * (S(i)*fabs(_ls.phi(i)) - phi_0(i));
                        local = true;
                    }
                }
                if(!local){
                    Vector phi_d = _hamiltonian_derivatives(_ls.phi, neighbors, i);
                    // Aproximate Hamiltonian.
                    for(unsigned j=0; j<phi_d.size(); j++){
                        if(S(i)>0.0 && j%2==0)      phi_d(j) = max(phi_d(j), 0.0);
                        else if(S(i)<0.0 && j%2==1) phi_d(j) = max(phi_d(j), 0.0);
                        else                        phi_d(j) = min(phi_d(j), 0.0);
                        phi_d(j) *= phi_d(j);
                    }
                    double H = sqrt(max(phi_d(0),phi_d(1)) + max(phi_d(2),phi_d(3)))-1.0;
                    phi_temp(i) -= S(i) * dtau * H;
                    if(fabs(H) > H_max) H_max = fabs(H);
                }
            }
            _ls.phi = phi_temp;
            itr++;
        }while(fabs(H_max) > 1e-8 && itr < 1000);
        std::cout<<"Reinitialization iters:"<<itr<<", H_max:"<<H_max<<"\n";
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Image segmentation.
    // See "LevelSet Evolution without Re-initialization: A New Variational Formulation", 2005 Li et al.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::segment_image(Vector &g, Region &bounds, bool implicit, unsigned steps,
            double lambda, double nu, double dt, double mu)
    {
        Vector f(_ls.size), laplacians(_ls.size), normal(_NDIM);
        Matrix gradients(_ls.size,_NDIM), normals(_ls.size,_NDIM);

        // Initialize the PDE solver for the implicit scheme.
        Helmholtz_Solver solver;
        double coeff(1.0/(dt*mu));
        string bcs = "NNNN";

        for(unsigned step=0; step<steps; step++){
            // Calculate all the laplacians, gradients, and normals.
            for(unsigned pt = 0; pt < _grid.rows(); pt++){
                Matrix del = _find_deltas(pt);
                laplacians(pt) = 0.0;
                for(unsigned dim = 0; dim < _NDIM; dim++){
                    if(!implicit) laplacians(pt) += _derivative(_ls.phi, del, pt, dim, true);
                    gradients(pt,dim) = _derivative(_ls.phi, del, pt, dim);
                    normal(dim) = gradients(pt,dim);
                }
                const double mag_norm = norm(normal);
                normal *= (mag_norm == 0.0 ? 0.0 : 1.0/mag_norm);
                normals.set_row(pt, normal);
            }
            Vector norm_x(normals.column(0)), norm_y(normals.column(1));
            // Calculate the values of phi.
            for(unsigned pt = 0; pt < _grid.rows(); pt++){
                Matrix del = _find_deltas(pt);
                // Calculate the divergence of the normal vectors.
                const double div_n = _derivative(norm_x,del,pt,0) + _derivative(norm_y,del,pt,1);
                // Calculate the divergence of the normal*g.
                double div_gn = _derivative(g,del,pt,0)*normals(pt,0)
                    + _derivative(norm_x,del,pt,0)*g(pt);
                div_gn += _derivative(g,del,pt,1)*normals(pt,1)
                    + _derivative(norm_y,del,pt,1)*g(pt);

                // Calculate the RHS.
                f(pt)  = lambda * _dirac_e(_ls.phi(pt)) * div_gn;
                f(pt) += nu * g(pt) * _dirac_e(_ls.phi(pt));
                if(!implicit) f(pt) += mu * (laplacians(pt)-div_n);
                else{
                    f(pt) *= 1.0/mu;
                    f(pt) += coeff*_ls.phi(pt) - div_n;
                }
            }
            if(!implicit){
                f *= dt;
                _ls.phi += f;
            }else{
                solver.init(bounds, bounds.get_max()(0), bounds.get_max()(1), bcs, coeff);
                _ls.phi = solver.solve(f,0.0,0.0,0.0,0.0);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the distances for the SDF.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::_calc_distances(double ds, double s_min, double pts, MinLSpts &min_ls_pts) {
        Matrix discrete_LS = _discretize_LS(ds, s_min, pts);
        Vector gridpt(_NDIM), lspt(_NDIM);
        double h = 0.1*ds;

        for(unsigned i=0; i<_grid.rows(); i++){
            gridpt = _grid.row(i);
            // Find the nearest discretized point on the interface.
            unsigned min_LS_pt(0);
            double min_dist2 = std::numeric_limits<double>::max(), dist2;
            for(unsigned j=0; j<discrete_LS.rows(); j++){
                const double dx2 = gridpt(0) - discrete_LS(j,0);
                const double dy2 = gridpt(1) - discrete_LS(j,1);
                dist2 = dx2*dx2 + dy2*dy2;
                if(dist2 < min_dist2){ min_dist2 = dist2; min_LS_pt = j; }
            }
            // Find the closest point on the interface, starting from min_LS_pt.
            double s = min_LS_pt * ds + s_min;
            for(unsigned j=0; j<25; j++){
                const double dist_l2 = gridpt.distance_squared(_ls_eqn(lspt,s-2*h));
                const double dist_l  = gridpt.distance_squared(_ls_eqn(lspt,s-h));
                const double dist_r  = gridpt.distance_squared(_ls_eqn(lspt,s+h));
                const double dist_r2 = gridpt.distance_squared(_ls_eqn(lspt,s+2*h));
                // Calculate the derivatives.
                const double newton = 2.0*h*(dist_r-dist_l)/(dist_r2+dist_l2);
                if(!isfinite(newton)) break;
                s -= newton;
                if(newton < TOL) break;
            }
            // Store the distance.
            _ls_eqn(lspt,s);
            _ls.phi(i) = gridpt.distance_to(lspt);
            min_ls_pts.s(i) = s;
            min_ls_pts.ls.set_row(i, lspt);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the signs for the SDF.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_PDE::_calc_signs(double ds, MinLSpts &min_ls_pts) {
        Vector signs(_ls.size,true);
        ds *= 0.5;
        std::set<unsigned> points;
        //TODO: this is a stupid hack, requires grid to be square and made with meshgrid.
        //  In our code we could use element_containing_point to find which element each point is in.
        const double dist = _grid.row(0).distance_to(_grid.row(sqrt(_grid.rows())+1));
        Vector normal(_NDIM), lspt(_NDIM);
        // Find the sign for each of the points with dist < (dx^2+dy^2)
        // Note: flipping the sign from the cross-product.
        for(unsigned i=0; i<_ls.size; i++){
            Vector gridpt = _grid.row(i);
            if(_ls.phi(i) <= dist){
                normal = cross(_ls_eqn(lspt,min_ls_pts.s(i)+ds) - min_ls_pts.ls.row(i),
                               gridpt - min_ls_pts.ls.row(i));
                signs(i) = normal(2) > 0.0 ? -1.0 : 1.0;
                points.insert(i);
            }
        }

        // Propagate the signs outward.
        while(!points.empty()){
            unsigned pt = *points.begin();
            vectorU32 &neighbors = _conn[pt];
            for(unsigned i=0; i<neighbors.size(); i++){
                if(signs(neighbors[i]) == 0){
                    signs(neighbors[i]) = signs(pt);
                    points.insert(neighbors[i]);
                }
            }
            points.erase(pt);
        }

        // Check to see if signs were found for all points.
        for(unsigned i=0; i<_ls.size; i++)
            if(signs(i) == 0.0){ std::cout<<"Warning: not all signs found.\n"; break; }
        _ls.phi *= signs;
    }
}

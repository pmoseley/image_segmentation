#include "Helmholtz_Solver.h"

namespace pde_solvers
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Constructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Helmholtz_Solver::Helmholtz_Solver() : _dpar(NULL), _f(NULL),
                                           _bd_ax(NULL), _bd_bx(NULL), _bd_ay(NULL), _bd_by(NULL)
    {}

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Deconstructor.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Helmholtz_Solver::~Helmholtz_Solver() {
        if(_dpar) free_Helmholtz_2D(&_xhandle, _ipar, &_stat);
        if(_stat < 0) error("2D Helmholtz free failed with error code "+utility::to_string(_stat));
        delete[] _dpar;
        delete[] _f;
        delete[] _bd_ax; delete[] _bd_bx;
        delete[] _bd_ay; delete[] _bd_by;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialize the Helmholtz equation solver.
    // bcs must be four characters long, each char either N or D for Neumann or Dirichlet bcs.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void Helmholtz_Solver::init(const Region &bounds, int nx, int ny, string &bcs, double coeff) {
        _nx = nx-1; _ny = ny-1;
        _xhandle = 0;
        // Allocate memory for dpar.
        _allocate_mem(_nx, _ny);

        const Vector min = bounds.get_min(), max = bounds.get_max();
        double ax(min(0)), bx(max(0)), ay(min(1)), by(max(1));
        d_init_Helmholtz_2D(&ax, &bx, &ay, &by, &_nx, &_ny, &bcs[0], &coeff,
                _ipar, _dpar, &_stat);
        if(_stat < 0) error("2D Helmholtz init failed with error code "+utility::to_string(_stat));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Solve the Helmholtz equation.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Vector Helmholtz_Solver::solve(Vector &f, Vector &bd_ax, Vector &bd_bx, Vector &bd_ay, Vector &bd_by) {
        delete[] _f;
        delete[] _bd_ax; delete[] _bd_bx;
        delete[] _bd_ay; delete[] _bd_by;
        _f=NULL; _bd_ax=NULL; _bd_bx=NULL; _bd_ay=NULL; _bd_by=NULL;
        _f = _Vector_to_double(f);
        _bd_ax = _Vector_to_double(bd_ax);
        _bd_bx = _Vector_to_double(bd_bx);
        _bd_ay = _Vector_to_double(bd_ay);
        _bd_by = _Vector_to_double(bd_by);

        d_commit_Helmholtz_2D(_f, _bd_ax, _bd_bx, _bd_ay, _bd_by, &_xhandle, _ipar, _dpar, &_stat);
        if(_stat < 0) error("2D Helmholtz commit failed with error code "+utility::to_string(_stat));

        d_Helmholtz_2D(_f, _bd_ax, _bd_by, _bd_ay, _bd_by, &_xhandle, _ipar, _dpar, &_stat);
        if(_stat < 0) error("2D Helmholtz solve failed with error code "+utility::to_string(_stat));

        return _double_to_Vector(_f);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Solve the Helmholtz equation, bcs are const.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Vector Helmholtz_Solver::solve(Vector &f, double bd_ax, double bd_bx, double bd_ay, double bd_by) {
        delete[] _f;
        delete[] _bd_ax; delete[] _bd_bx;
        delete[] _bd_ay; delete[] _bd_by;
        _f=NULL; _bd_ax=NULL; _bd_bx=NULL; _bd_ay=NULL; _bd_by=NULL;
        _f = _Vector_to_double(f);
        _bd_ax = new double[_ny+1];
        _bd_bx = new double[_ny+1];
        _bd_ay = new double[_nx+1];
        _bd_by = new double[_nx+1];
        for(unsigned i=0; i<_nx+1; i++){
            _bd_ax[i] = bd_ax;
            _bd_bx[i] = bd_bx;
            _bd_ay[i] = bd_ay;
            _bd_by[i] = bd_by;
        }

        d_commit_Helmholtz_2D(_f, _bd_ax, _bd_bx, _bd_ay, _bd_by, &_xhandle, _ipar, _dpar, &_stat);
        if(_stat < 0) error("2D Helmholtz commit failed with error code "+utility::to_string(_stat));

        d_Helmholtz_2D(_f, _bd_ax, _bd_by, _bd_ay, _bd_by, &_xhandle, _ipar, _dpar, &_stat);
        if(_stat < 0) error("2D Helmholtz solve failed with error code "+utility::to_string(_stat));

        return _double_to_Vector(_f);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Convert matcad::Vector to double*
    ////////////////////////////////////////////////////////////////////////////////////////////////
    double* Helmholtz_Solver::_Vector_to_double(Vector &vec) {
        // This works for meshes created by meshgrid.
        //  Value for gridpoint (i,j) should be stored in array(i+j(nx+1))
        double *array = new double[vec.size()];
        for(unsigned i=0; i<vec.size(); i++) array[i] = vec(i);
        return array;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Convert double* to matcad::Vector
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Vector Helmholtz_Solver::_double_to_Vector(double* array) {
        // This works for meshes created by meshgrid.
        //  Value for gridpoint (i,j) should be stored in array(i+j(nx+1))
        Vector vec((_nx+1)*(_ny+1));
        for(unsigned i=0; i<vec.size(); i++) vec(i) = array[i];
        return vec;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Allocate memory.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void Helmholtz_Solver::_allocate_mem(int nx, int ny) {
        // Clear the arrays.
        for(unsigned i=0; i<128; i++) _ipar[i]=0;
        delete[] _dpar;
        _dpar = new double[5*nx/2 + 7];
    }
}

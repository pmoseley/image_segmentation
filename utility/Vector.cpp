#include "Vector.h"
#include "Matrix.h"
#include "utility.h"
#include <sstream>
#include <cmath>

namespace matcad {
    ////////////////////////////////////////////////////////////////////////////
    // Changes the length of this vector, optionally copies over what fits.
    ////////////////////////////////////////////////////////////////////////////
    Vector& Vector::resize(unsigned length, bool copy)
    {
        // Note: Vector::resize does not always make Vector unique.
        if (size() == length) return *this;
        SharedArray<Real> *buff = new SharedArray<Real>(length);
        // Copies over old data, and zero pad extra elements.
        if (copy && size() && length) {
            unsigned i;
            for (i=0; i<std::min(length, size()); i++)  (*buff)[i]=(*_data)[i];
            for (; i<length; i++)                       (*buff)[i]=0.0;
        }
        SharedArray<Real>::delete_reference(_data);
        _data = buff;
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Sets all components to zero, optionally resizes the vector.
    ////////////////////////////////////////////////////////////////////////////
    Vector& Vector::zero(unsigned length)
    {
        if (~length && length!=size())   resize(length);
        _data->assign(0.0);
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Computes the dot product (element-wise).
    ////////////////////////////////////////////////////////////////////////////
    Real dot(const Vector &x, const Vector &y)
    {
        if (INDEX_CHECK && !x.same_size(y))
            error("dot product: vectors must be same length");
        int m = (int)x.size();
        switch (m) {
        case 0: return 0.0;
        case 1: return x(0)*y(0);
        case 2: return x(1)*y(1)+x(0)*y(0);
        case 3: return x(2)*y(2)+x(1)*y(1)+x(0)*y(0);
        case 4: return x(3)*y(3)+x(2)*y(2)+x(1)*y(1)+x(0)*y(0);
        case 5: return x(4)*y(4)+x(3)*y(3)+x(2)*y(2)+x(1)*y(1)+x(0)*y(0);
        case 6: return x(5)*y(5)+x(4)*y(4)+x(3)*y(3)
                      +x(2)*y(2)+x(1)*y(1)+x(0)*y(0);
        default: break;
        }
        int inc=1;
        return ddot(&m, x._data->_data, &inc, y._data->_data, &inc);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Writes this vector to a output stream.
    ////////////////////////////////////////////////////////////////////////////
    std::ostream& operator<<(std::ostream &os, const Vector &v)
    {
        return (os << v.to_string("\t", "\n"));
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Used to output the vector to a binary stream
    /////////////////////////////////////////////////////////////////////////////////////////////////
    unsigned Vector::binary_write(const std::string &filename, bool append) const
    {
        using namespace::std;
        ios::openmode mode(ios::out | ios::binary | (append ? ios::app : ios::trunc));
        fstream fid(filename.c_str(), mode);
        return binary_write(fid);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Used to output the vector to a binary stream at a specified floating point precision
    /////////////////////////////////////////////////////////////////////////////////////////////////
    unsigned Vector::binary_write(std::fstream &fid) const
    {
        unsigned length = size();
        fid.write((char*)&length, sizeof(unsigned));
        fid.write((const char*)(_data->begin()), sizeof(double)*length);
        return sizeof(double)*length + sizeof(unsigned);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Reads a vector from a delimited text file
    /////////////////////////////////////////////////////////////////////////////////////////////////
    int Vector::binary_read(std::fstream &fid)
    {
        _unique();
        unsigned length;
        fid.read((char*)&length, sizeof(unsigned));
        if (!fid) error("Error after reading binary vector file");
        resize(length);
        fid.read((char *)_data->begin(), sizeof(double)*length);
        return length;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    // Reads a vector from a delimited text file
    /////////////////////////////////////////////////////////////////////////////////////////////////
    int Vector::binary_read(const std::string &fileName)
    {
        std::fstream fid(fileName.c_str(), std::ios::in|std::ios::binary);
        if (!fid) error("Error reading file: " + fileName);
        return binary_read(fid);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Outputs the vector as a string.
    ////////////////////////////////////////////////////////////////////////////
    string Vector::to_string(const string &delimiter, const string &eol) const
    {
        std::ostringstream os;
        for (unsigned i=0; i<size(); i++) {
            if (i) os << delimiter;
            os << (*this)(i);
        }
        return os.str() + eol;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Outputs the vector as a string.
    ////////////////////////////////////////////////////////////////////////////
    Vector rand(unsigned length)
    {
        Vector y(length);
        for (unsigned i=0; i<length; i++) y(i) = utility::drand();
        return y;
    }
}

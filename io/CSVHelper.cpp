#include "CSVHelper.h"

namespace matcad {
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Read in edge indicator function.
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    EdgeIndicator read_csv(const string &fname) {
        std::ifstream file;
        file.open(fname.c_str());
        if(!file.is_open()) warning("Unable to open edge indicator file: " + fname);

        // Find out how many rows and columns.
        unsigned nrows(1), ncols(0), posL(0), posR(0);
        string line;
        std::getline(file, line);
        while(posL != -1){
            posL = line.find(',',posL+1);
            ncols++;
        }
        while(std::getline(file,line)) nrows++;

        // Create the edge indicator object.
        Vector min(0.0,0.0), max((double)nrows,(double)ncols);
        EdgeIndicator ei(min,max);
        ei.g.zero(nrows*ncols);
        // Extract the data from the file, using the same ordering as used to create the mesh.
        file.clear(); file.seekg(0);
        for(unsigned i=0, idx=0; i<nrows; i++){
            std::getline(file, line);
            posL = 0;
            for(unsigned j=0; j<ncols; j++){
                posR = line.find(',',posL);
                ei.g(idx) = utility::from_string<double>(line.substr(posL,posR));
                posL = posR + 1;
                idx++;
            }
        }
        file.close();
        // Create the grid.
        meshgrid(ei, true);

        return ei;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Create a matrix containing the x and y coordinates.
    // This could be easily generalized for 3d.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void meshgrid(EdgeIndicator &ei, bool periodic) {
        const Vector &bmax = ei.bounds.get_max();
        unsigned xN(bmax(0)), yN(bmax(1));
        ei.grid = Matrix(bmax(0)*bmax(1), ei.bounds.num_dim());
        ei.conn.clear();
        const double dx=ei.bounds.range(0)/(xN-1), dy=ei.bounds.range(1)/(yN-1);
        const Vector min = ei.bounds.get_min(), max = ei.bounds.get_max();
        // Grid starts from upper-left and increments horizontally first.
        for(unsigned j=0, idx=0; j<yN; j++){
            for(unsigned i=0; i<xN; i++){
                ei.grid(idx,0) = i*dx + min(0);
                ei.grid(idx,1) = max(1) - j*dy;
                vectorU32 nbrs;
                if(i > 0)           nbrs.push_back(idx-1);
                else if(periodic)   nbrs.push_back(idx+xN-1);   // Point on xmin.
                if(i < xN-1)        nbrs.push_back(idx+1);
                else if(periodic)   nbrs.push_back(idx-xN+1);   // Point on xmax.
                if(j > 0)           nbrs.push_back(idx-xN);
                else if(periodic)   nbrs.push_back(idx+(yN-1)*xN);  // Point on ymax
                if(j < yN-1)        nbrs.push_back(idx+xN);
                else if(periodic)   nbrs.push_back(idx-(yN-1)*xN);  // Point on ymin
                ei.conn.push_back(nbrs);
                idx++;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Writes out datafile for plotting with Matlab or GnuPlot.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void write_csv(string filename, Vector &data, bool circular){
        filename += ".csv";
        std::ofstream file;
        file.open(filename.c_str());
        file << data.to_string("\n");
        if(circular) file << data(0) << "\n";   // Reprint the first datapoint to connect the loop.
        file.close();
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Writes out datafile for plotting with Matlab or GnuPlot.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void write_csv(string filename, Matrix &data, bool circular){
        filename += ".csv";
        std::ofstream file;
        file.open(filename.c_str());
        file << data;
        if(circular) file << data.row(0);   // Reprint the first datapoint to connect the loop.
        file.close();
    }

}

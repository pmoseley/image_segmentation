function postprocess(image_file, grid_file, ls_file_prefix)
% Import the files.
img = imread(image_file);
grid = importdata(grid_file);
phi_files = read_phi(ls_file_prefix);

% TODO - only works for square grids.
x = grid(:,1); y = grid(:,2); clear grid;
x = x(1:sqrt(size(x))); y = y(1:sqrt(size(y)):end);
% Plot the levelset values.
figure; imshow(img); hold on;
for(i=1:size(phi_files,1))
    phi = importdata(phi_files(i).name);
    phi = rot90(reshape(phi, sqrt(length(phi)), sqrt(length(phi))));
    if(i==size(phi_files,1))
        contour(x,y,phi,[0,0],'r');
    else
        contour(x,y,phi,[0,0],'b');
    end
end
hold off;

% Function to fix dir output, sorts in proper numerical order.
function phi_files = read_phi(ls_file_prefix)
    % Read all the file names.
    phi_files = dir(strcat(ls_file_prefix,'*.csv'));
    % Extract the numbers from the files.
    filenums = [];
    for(i=1:size(phi_files,1))
        num = str2num(char(regexp(phi_files(i).name,'[0-9]*','match')));
        filenums = [filenums; [num, i]];
    end
    % Resort the filenames in proper numerical order.
    filenums = sortrows(filenums,1);
    phi_files = phi_files(filenums(:,2));

#include "utility.h"
#include <mkl.h>

namespace utility {

    // Computes a random real number between min_x and max_x.
    double drand(double min_x, double max_x, unsigned seed)
    {
        if (~seed) srand(seed);
        static double D=1.0/double(RAND_MAX);
        return (D*double(rand()))*(max_x-min_x)+min_x;
    }
}

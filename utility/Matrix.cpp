#include "Matrix.h"
#include "utility.h"
#include <sstream>

namespace matcad {
    ///////////////////////////////////////////////////////////////////////////////
    // Changes the length of matrix. Optional: copy what fits, sets rest to zero.
    ///////////////////////////////////////////////////////////////////////////////
    Matrix& Matrix::resize(unsigned nrow, unsigned ncol, bool copy)
    {
        if (is_size(nrow, ncol)) return *this;  // Don't need to do anything.
        if (copy && _t) _transpose_storage();   // This is inefficient but rare.
        _t = 0;                                 // No copy, disregard _t flag.
        if (size()!=nrow*ncol || copy) {        // Resize data, unless copying.
            SharedArray<Real> *buff = new SharedArray<Real>(nrow*ncol);
            if (copy) {
                const unsigned col_to_copy=std::min(ncol, cols());
                const unsigned row_to_copy=std::min(nrow, rows());
                for (unsigned j=0; j<col_to_copy; j++)  {
                    _data->copy_chunk(*buff, j*rows(), j*nrow, row_to_copy);
                    // Sets remainder of column to 0.0.
                    for (unsigned i=row_to_copy; i<nrow; i++)
                        (*buff)[j*nrow + i] = 0.0;
                }
                // Sets any new columns to 0.0.
                for (unsigned j=col_to_copy; j<ncol; j++) {
                    for (unsigned i=0; i<nrow; i++)
                        (*buff)[j*nrow + i] = 0.0;
                }

            }
            SharedArray<Real>::delete_reference(_data);
            _data = buff;
        }
        //else _data->unique();                 // not sure this is necessary
        _rows = nrow;
        _cols = ncol;
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Writes this vector to a output stream.
    ////////////////////////////////////////////////////////////////////////////
    std::ostream& operator<<(std::ostream &os, const Matrix &v)
    {
        return (os << v.to_string("\t", "\n"));
    }
    ////////////////////////////////////////////////////////////////////////////
    // Outputs the vector as a string.
    ////////////////////////////////////////////////////////////////////////////
    string Matrix::to_string(const string &delimiter, const string &eol) const
    {
        std::ostringstream os;
        for (unsigned i=0; i<rows(); i++) {
            for (unsigned j=0; j<cols(); j++) {
                if (j) os << delimiter;
                os << MATRIX_DATA(i,j);
            }
            os << eol;
        }
        return os.str();
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns a matrix with random values between (0,1)
    ////////////////////////////////////////////////////////////////////////////
    Matrix rand(unsigned rows, unsigned cols)
    {
        Matrix R(rows, cols);
        for (unsigned j=0; j<R.cols(); j++)
            for (unsigned i=0; i<R.rows(); i++) R(i,j) = utility::drand();
        return R;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the inverse of matrix and optionally returns the determinant.
    ////////////////////////////////////////////////////////////////////////////
    Matrix inv(const Matrix &A, double *detA)
    {
        switch (A.rows()) {
        case 3: return inv3x3(A, detA);
        case 2: return inv2x2(A, detA);
        case 1: return inv1x1(A, detA);
        case 0: return Matrix();
        default:
            Matrix B(A);
            double *a = B.begin();
            int m(A.rows()), info, *ipiv(new int[m<<1]);
            int *iwork=ipiv+m;

            // compute LU factorization
            dgetrf(&m, &m, a, &m, ipiv, &info);
            if (INDEX_CHECK && info<0)
                error("inv(Matrix) dgetrf error: bad argument");
            if (INDEX_CHECK && info>0)
                error("inv(Matrix) dgetrf error: matrix is singular");

            // if we need the determinant of A
            if (detA) {
                static double sign[] = {-1.0, 1.0};
                *detA = 1.0;
                int odd_pivot_count = ipiv[0] != 1;
                for (int i=0; i<m; i++) {
                    *detA *= B(i,i);
                    odd_pivot_count += (ipiv[i] != (i+1));
                }
                *detA *= sign[odd_pivot_count&1];
            }

            // compute 1-norm of original matrix (for cond number estimate)
            char norm = '1';
            double rcond, *workc(new double[m<<2]);
            double anorm(dlange(&norm, &m, &m, a, &m, workc));

            // estimate condition number (warn if bad)
            dgecon(&norm, &m, a, &m, &anorm, &rcond, workc, iwork, &info);
            if (INDEX_CHECK && info<0)
                error("inv(Matrix): dgecon error: bad argument");
            if (rcond < 1.0e-14)
                warning("inv(Matrix): matrix nearly singular, RCOND<1e-14");

            // new determine optimal work size for computation of matrix inverse
            double work_dummy[2] = {0.0, 0.0};
            int lwork=-1;
            dgetri(&m, a, &m, ipiv, work_dummy, &lwork, &info);
            if (INDEX_CHECK && info<0)
                error("inv(Matrix) dgetri error: bad argument");

            // work size query succeeded
            double *work(new double[lwork = (int)work_dummy[0]]);

            // compute matrix inverse
            dgetri(&m, a, &m, ipiv, work, &lwork, &info);
            if (INDEX_CHECK && info<0)
                error("inv(Matrix) detri error: bad argument");
            if (INDEX_CHECK && info>0)
                error("inv(Matrix) detri error: matrix is singular");

            delete [] ipiv;
            delete [] work;
            delete [] workc;
            return B;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the determinant of a square matrix.
    ////////////////////////////////////////////////////////////////////////////
    Real det(const Matrix &A)
    {
        if (INDEX_CHECK && !A.square())
            error("determinate takes a square matrix");
        int m = A.rows();
        switch (m) {
        case 0: return 0.0;
        case 1: return A(0,0);
        case 2: return A(0,0)*A(1,1)-A(0,1)*A(1,0);
        case 3: return A(0,0)*(A(1,1)*A(2,2)-A(1,2)*A(2,1))
                     + A(0,1)*(A(1,2)*A(2,0)-A(1,0)*A(2,2))
                     + A(0,2)*(A(1,0)*A(2,1)-A(1,1)*A(2,0));
        default: break;
        }
        static const double sign[] = {-1.0, 1.0};
        int info, *ipiv(new int[m]);
        double det(1.0);
        Matrix LU(A);
        dgetrf(&m, &m, LU.begin(), &m, ipiv, &info);
        if (INDEX_CHECK && info<0)
            error("det(Matrix): detrf error, bad argument value");
        int odd_pivot_count = ipiv[0] != 1;
        for (int i=0; i<m; i++) {
            det *= LU(i,i);
            odd_pivot_count += (ipiv[i] != (i+1));
        }
        det *= sign[odd_pivot_count&1];
        delete [] ipiv;
        return det;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Computes the eigenvalues of the real matrix.
    ////////////////////////////////////////////////////////////////////////////
    Vector eig(const Matrix &A)
    {
        if (INDEX_CHECK && !A.square()) error("eig() requires square matrix.");
        int n=A.rows(), ilo[]={1}, info, lwork=A.rows();

        // Makes a copy of matrix A and stores it in H.
        std::vector<double> tau(n, 0.0), Z(n, 0.0), work(n, 0.0);
        Matrix H(A);
        dgehrd(&n, ilo, &n, H.begin(), &n, &tau[0], &work[0], &lwork, &info);
        // Real and imaginary parts of the eigenvalues.
        Vector wr(n), wi(n);
        static char job[]="E", compz[]="N";
        dhseqr(job, compz, &n, ilo, &n, H.begin(), &n, wr.begin(), wi.begin(),
               &Z[0], &n, &work[0], &lwork, &info);
        return wr;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the matrix-matrix product: A*B.
    ////////////////////////////////////////////////////////////////////////////
    Matrix operator*(const Matrix &A, const Matrix &B)
    {
        if (INDEX_CHECK && A.cols()!=B.rows())
            error("Matrix-matrix multiplication size mismatch");
        Matrix C(A.rows(), B.cols());
        if (BLAS_ENABLED) {
            static const char tr[] = "NT";
            const char *At=tr+A._t, *Bt=tr+B._t;
            int mkn[]={A.rows(), A.cols(), B.cols()};
            double alpha[]={1.0}, beta[]={0.0};

            dgemm(At, Bt, mkn, mkn+2, mkn+1, alpha,
                  A.begin(), mkn+A._t,         // Lead dim of A is either m,k.
                  B.begin(), mkn+1+B._t, beta, // Lead dim of A is either k,n.
                  C.begin(), mkn);             // Lead dim of C is m.
        }
        else {
            for (unsigned i=0; i<C.rows(); i++) {
                for (unsigned j=0; j<C.cols(); j++) {
                    C(i,j) = A(i,0)*B(0,j);
                    for (unsigned k=1; k<A.cols(); k++) C(i,j) += A(i,k)*B(k,j);
                }
            }
        }
        return C;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the matrix vector product: A*x.
    ////////////////////////////////////////////////////////////////////////////
    Vector operator*(const Matrix &A, const Vector &x)
    {
        if (INDEX_CHECK && A.cols()!=x.size())
            error("Matrix vector multiplication size mismatch");
        Vector y(A.rows());
        if (BLAS_ENABLED) {
            int m[]={A.rows()}, n[]={A.cols()}, inc[]={1};
            double alpha[]={1.0}, beta[]={0.0};
            static const char tr[] = "NT"; // default is Not tranpose
            const char *t = tr+A._t;       // A tranpose flag true leads to t="T"
            dgemv(t, m, n, alpha, A.begin(), m, x.begin(), inc, beta, y.begin(), inc);
        }
        else {
            for (unsigned i=0; i<A.rows(); i++) y(i)=A(i,0)*x(0);
            for (unsigned j=1; j<A.cols(); j++)
                for (unsigned i=0; i<A.rows(); i++) y(i)+=A(i,j)*x(j);
        }
        return y;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the tranpose-matrix vector product: A'x.
    ////////////////////////////////////////////////////////////////////////////
    Vector operator*(const Vector &x, const Matrix &A)
    {
        if (INDEX_CHECK && A.rows()!=x.size())
            error("Vector-matrix multiplication size mismatch");
        Vector y(A.cols());
        if (BLAS_ENABLED) {
            int m=A.rows(), n=A.cols(), inc=1;
            double alpha=1.0, beta=0.0;
            static const char tr[] = "TN";  // default transpose (A'x)
            const char *t = tr+A._t;        // transpose flag changes t to "N"
            dgemv(t, &m, &n, &alpha, A.begin(), &m, x.begin(), &inc,
                  &beta, y.begin(), &inc);
        }
        else {
            for (unsigned i=0; i<A.rows(); i++) {
                y(i) = A(0,i)*x(0);
                for (unsigned j=1; j<A.cols(); j++) y(j) += A(j,i)*x(i);
            }
        }
        return y;
    }
}


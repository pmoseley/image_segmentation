#pragma once
#define LOG (utility::logger())
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <vector>

namespace utility {
    class Logger {
        friend Logger& logger();
        Logger();                   //!< Default constructor, cannot be publically called.

    public:
        //! Opens a file for logging.
        std::string open_file(const std::vector<std::string> &cmd);
        //! Set the verbosity.
        std::string set_verbosity(const std::vector<std::string> &cmd);
        //! Sets the logger to be quiet (i.e. no console writes).
        void set_quiet(int quiet=1) { _quiet = quiet; }
        //! Writes a message to the log and to console.
        template<typename T> void out(const T &t, int verb=1, bool eol=true){
            if(verb <= _verb){
                if (!_quiet){
                    std::cout << t << (eol ? "\n" : "");
                    std::cout.flush();
                }
                if(_logfile.is_open()){
                    _logfile << t << (eol ? "\n" : "");
                    _logfile.flush();
                }
            }
        }

    private:
        std::ofstream _logfile;     //!< Logfile for output.
        int _verb;                  //!< Verbosity level.
        int _quiet;                 //!< If true, no writing to console.
    };



    //! Initializes the logger or returns a reference to it. Must be defined in a cpp file.
    Logger& logger();

    //! Generic output command.
    template <typename T> static Logger& operator<<(Logger &o, const T &t) {
        o.out(t, 1, false);
        return o;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Displays an error message and quits.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    static void error(const std::string &msg, const char *file=NULL, int line=-1)
    {
        std::string err_msg = "ERROR: ";
        if (file)             err_msg += "-" + std::string(file);
        if (file && line>=0) err_msg += ':';
        if (line>=0)          err_msg += line;
        if (file || line>=0) err_msg += '\n';
        LOG.out(err_msg + msg, 0);
        exit(1);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Writes a warning message, returns an int so warnings can be 1x only.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    static int warning(const std::string &msg)
    {
        LOG.out("WARNING: "+msg,1);
        return 0;
    }
}

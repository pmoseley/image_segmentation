#pragma once
#include "Matrix.h"
#include "Vector.h"

namespace matcad {
    //! Defines a rectangular region in nD space bounded by two points
    //! such that a point in the region satisfies pt1(i) < x(i) < pt2(i).
    class Region {
    public:
        //! Constructs a rectangular region bounded by two corner points.
        Region(const Vector &a, const Vector &b);
        //! Returns the number of dimensions.
        unsigned num_dim() const;
        //! Returns true if x falls within region with tolerance, tol.
        bool point_in_region(const Vector &x, double tol=1.0e-6) const;
        //! Returns the length of the region along a dimension, i.
        double range(unsigned i) const;

        //! Sets the minimum point along each dimension.
        void set_min(Vector &a);
        //! Sets the maximum point along each dimension.
        void set_max(Vector &a);
        //! Returns a const reference to the minimum value.
        const Vector& get_min() const { return _min; }
        //! Returns a const reference to the maximum value.
        const Vector& get_max() const { return _max; }
    private:
        //! Enforces that _min(i) < _max(i)
        void _check_min_max();
        Vector _min; //!< Minimum on each dimension.
        Vector _max; //!< Maximum on each dimension.
    };

    //! Returns true if (x0-TOL) <= x <= (x1+TOL).
    template<class T>
    bool in_range(const T &x0, const T &x1, const T &x, T TOL=T(0));

    //! Find the boundaries of a matrix, uses subset if given indexes.
    Region find_bounds(const Matrix &x, const vectorU32 &idx=vectorU32());
}
#include "Region-inl.h"


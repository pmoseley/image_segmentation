namespace matcad
{
    /////////////////////////////////////////////////////////////////////////////////////////////////
    //! function that keeps track of amount of data used in matrices and vectors
    /////////////////////////////////////////////////////////////////////////////////////////////////
    int array_storage_count(int change)
    {
        static int array_memory_usage = 0;
        array_memory_usage += change;
        return array_memory_usage;
    }
}

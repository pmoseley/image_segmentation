#pragma once
#pragma warning( disable : 4267 )
#include <algorithm>
#include <cctype>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <vector>
#include "Vector.h"

namespace utility
{
    using std::string;
    using std::vector;
    using std::fstream;
    typedef std::vector<std::string> Command;

    ///////////////////////////////////////////////////////////////////////////////
    //! Converts a string to anything
    ///////////////////////////////////////////////////////////////////////////////
    template <typename T>
    inline T from_string(const string &s)
    {
        std::istringstream ss(s);
        T t;
        if (!(ss >> t)) warning("Error bad conversion from a string: " + s + "\n");
        return t;
    }
    //! Converts a string to a double - because full template above is annoying
    inline double str2dbl(const string &s)
    {
        return from_string<double>(s);
    }
    ///////////////////////////////////////////////////////////////////////////////
    //! Converts a string to a vector
    ///////////////////////////////////////////////////////////////////////////////
    inline matcad::Vector str2vct(const string &s)
    {
        std::istringstream in(s);
        double x;
        matcad::Vector v;
        while (in >> x)        {
            v.resize(v.size()+1, true);
            v(v.size()-1) = x;
        }
        return v;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Converts a string to a stl::vector of type T
    ///////////////////////////////////////////////////////////////////////////////
    template <typename T>
    inline std::vector<T> str2list(const string &s)
    {
        std::istringstream in(s);
        T item;
        std::vector<T> v;
        while (in >> item)    v.push_back(item);
        return v;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Reads a matlab-like range (e.g. 1:2:8) and returns min, max, and increment
    ///////////////////////////////////////////////////////////////////////////////
    template <typename T>
    static void str2range(const string &s, T& min, T& max, T& inc)
    {
        size_t L = s.length();    // string length
        const size_t c1 = s.find(':');
        const size_t c2 = s.find(':', c1);

        if (!c1) error("found at beginning of range");
        min = from_string<T>(s.substr(0,c1));
        inc = c2>L ? 1 : from_string<T>(s.substr(c1+1, c2-c1));
        if (c1>L)    max = min;
        else        max = from_string<T>(s.substr(1+(c2>L?c1:c2)));
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Converts anything to a string
    ///////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline string to_string(const T& x, int width=-1, int precision=-1)
    {
        std::ostringstream o;
        if (width >= 0)         o<<std::setw(width)<<std::setfill('0');
        if (precision >= 0)  o<<std::fixed<<std::setprecision(precision);
        o<<x;
        return o.str();
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Supplies a numbered entry name
    ///////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline string NName(const string &s, const T& d, int width=0)
    {
        return s + to_string(d, width);
    }
    ///////////////////////////////////////////////////////////////////////////////
    //! replaces all characters in a set of characters with a character
    //! @param str input string
    //! @param *s pointer to array of characters that will be replaced
    //! @param r character to replace characters in s[] with
    ///////////////////////////////////////////////////////////////////////////////
    static bool replace_all_of(string &str, const char *s, const char r)
    {
        bool replaced(false);
        size_t found;
        found = str.find_first_of(s);
        while (found != string::npos)
        {
            str[found] = r;
            found = str.find_first_of(s, found+1);
            replaced = true;
        }
        return replaced;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Converts a string to lowercase
    ///////////////////////////////////////////////////////////////////////////////
    inline string &to_lower(string &s)
    {
        std::transform(s.begin(),s.end(),s.begin(),static_cast < int(*)(int) > (tolower));
        return s;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Converts a string to lowercase
    ///////////////////////////////////////////////////////////////////////////////
    inline string &to_upper(string &s)
    {
        std::transform(s.begin(),s.end(),s.begin(),static_cast < int(*)(int) > (toupper));
        return s;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Removes any whitespace from end or beginning of string
    ///////////////////////////////////////////////////////////////////////////////
    inline string& trim(string &x)
    {
        if (x.size()==0) return x;
        x.erase(x.find_last_not_of(" \t\n")+1);
        return x = x.substr(x.find_first_not_of(" \t\n"));
    }
    ///////////////////////////////////////////////////////////////////////////////
    //! splits delimited string into a vector of strings
    ///////////////////////////////////////////////////////////////////////////////
    static void split_up_string(const string &s, vector<string> &ss, char del=' ')
    {
        ss.clear();
        size_t begin=0, end=0;
        while (end != string::npos)
        {
            begin = s.find_first_not_of(del, end);     // find beginning of fragment
            end = s.find_first_of(del,begin);
            if (begin != string::npos)                 // safe if end is npos-1
                ss.push_back(s.substr(begin,end-begin));
        }
    }
    ///////////////////////////////////////////////////////////////////////////////
    //! reads a string and deletes comments started a character
    ///////////////////////////////////////////////////////////////////////////////
    inline string read_line(fstream &fid, char comment='#')
    {
        string line;
        getline(fid, line);
        if (line.find(comment) != string::npos)  line.erase(line.find(comment));
        return line;
    }
    ///////////////////////////////////////////////////////////////////////////////
    //! scans a string for a list of commands delimited by ', \t' with # comments
    //! @param line The input line to be parsed
    //! @cs A vector of strings parsed from the input line
    ///////////////////////////////////////////////////////////////////////////////
    static vector<string> break_up_command(string line)
    {
        vector<string> cs;
        if (line.find('#') != string::npos)  line.erase(line.find("#"));
        replace_all_of(line, "\t,", ' ');
        to_lower(trim(line));
        split_up_string(line, cs);
        return cs;
    }
    ///////////////////////////////////////////////////////////////////////////////
    // Returns true if substring exists in string, if use_case do not ignore case
    ///////////////////////////////////////////////////////////////////////////////
    inline bool IsInString(const string &str, const string &substr, bool use_case=1)
    {
        if (use_case)    return str.find(substr) != string::npos;
        string STR(str), SUBSTR(substr);
        to_upper(STR);
        to_upper(SUBSTR);
        return STR.find(SUBSTR) != string::npos;
    }
    //////////////////////////////////////////////////////////////////////////////
    //! reads a single line from a file and splits it into a vector of strings
    //! returns the number of strings in the vector
    /////////////////////////////////////////////////////////////////////////////
    inline unsigned get_command(fstream &fid, Command &cmd)
    {
        string line;
        getline(fid, line);
        cmd = break_up_command(line);
        return (unsigned)cmd.size();
    }
    //////////////////////////////////////////////////////////////////////////////
    //! flattens the command to a string, default to size
    //////////////////////////////////////////////////////////////////////////////
    static string flatten_command(const Command &cmd, unsigned first=0, unsigned length=~0)
    {
        string flat(cmd[first]);
        if (length==~0) length=cmd.size() - first;
        for (unsigned i=first+1; i<cmd.size(); i++) flat += " " + cmd[i];
        return flat;
    }
    //////////////////////////////////////////////////////////////////////////////
    //! extracts a vector from a command
    //////////////////////////////////////////////////////////////////////////////
    static matcad::Vector get_vec_from_cmd(const Command &cmd, unsigned v0, unsigned length=~0)
    {
        length = std::min(length, (unsigned)cmd.size()-v0);    // default - go to end
        matcad::Vector v(length);
        if (cmd.size() < v0+length) error("command is too small to extract vector");
        for (unsigned i=0; i<length; i++)    v(i) = from_string<double>(cmd[v0+i]);
        return v;
    }
  ////////////////////////////////////////////////////////////////////////////
  // Format time nicely for output.
  ////////////////////////////////////////////////////////////////////////////
  template<class T>
  std::string format_time(T seconds)
  {
    int t = unsigned(seconds);
    string time, time_fmt;
    if (t>=3600) {
      time += to_string(t/3600,2) + ":";
      time_fmt += "hh:";
      t %= 3600;
    }
    if (t>=60) {
      time += to_string(t/60,2) + ":";
      time_fmt += "mm:";
      t %= 60;
    }
    // preserve decimal point
    seconds -= floor(seconds/60.0)*60.0;
    time += to_string(seconds, 2, 2);
    if (time_fmt.empty()) time_fmt = "sec";
    else time_fmt += "ss";
    return time + " " + time_fmt;
  }
}


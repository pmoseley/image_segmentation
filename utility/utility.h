#pragma once
#pragma warning( disable : 4267 )    // no warning for converting size_t to unsigned

#include <iostream>
#include <vector>
#include <ctime>
#include <cmath>
#include <stdlib.h>
#include <mkl.h>

namespace utility {
    //! Computes a random real number between min_x and max_x.
    double drand(double min_x=0.0, double max_x=1.0, unsigned seed=~0);
    //! Returns the sign of a number
    template<typename T> inline T Sign(T x)    { return (x >= 0) ? 1 : -1; }
    //! Returns the pythagorean distance with the two lengths
    template<typename T> inline T norm(T x, T y) { return sqrt(x*x+y*y); }
    //! Returns a x^2+y^2
    template<typename T> inline T x2py2(T x, T y) { return x*x + y*y; }
    //! Returns square of a value
    template<typename T> inline T square(T x) { return x*x; }

    // Makes sure that v is between min and max, doesn't check if min < max
    template<typename T> inline T ClipValue(const T &v, const T &min, const T &max)
    {
        return (v<min) ? min : ((v>max) ? max : v);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns true if the value v is between min and max
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline bool InRange(const T &v, const T &min, const T &max)
    {
        return v >= min && v <= max;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns true if the value v is between min and max within the tolerance TOL
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline bool InRange(const T &v, const T &min, const T &max, const T &TOL)
    {
        return InRange(v, min-TOL, max+TOL);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns the value with the larger absolute value
    ///////////////////////////////////////////////////////////////////////////////////////////////
    inline double MaxAbs(const double &a, const double &b)
    {
        return (fabs(a) > fabs(b)) ? a : b;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns the value with the smaller absolute value
    ///////////////////////////////////////////////////////////////////////////////////////////////
    inline double MinAbs(const double &a, const double &b)
    {
        return (fabs(a) < fabs(b)) ? a : b;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // If a and b are the same sign, return the one with the smallest absolute value, else return 0
    ///////////////////////////////////////////////////////////////////////////////////////////////
    inline double MinMod(const double &a, const double &b)
    {
        return 0.5*(Sign(a) + Sign(b)) * std::min(fabs(a),fabs(b));
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns the sum of a vector
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline T Sum(std::vector<T> v)
    {
        if (v.size()==0) return (T)0;
        T value = v[0];
        for (unsigned i=1; i<v.size(); i++)    value += v[i];
        return value;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns the sum of a vector
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<typename T>
    inline T STLvectorDot(std::vector<T> v1, std::vector<T> v2)
    {
        if (v1.size()==0 || v1.size()!=v2.size()) return (T)0;
        T value = v1[0]*v2[0];
        for (unsigned i=1; i<v1.size(); i++)    value += v1[i]*v2[i];
        return value;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // Linear binary search algorithm, works with either sorted or unsorted
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    inline int SearchList(std::vector<unsigned> A, unsigned value, bool sorted=false)
    {
        if (!sorted) {
            for (unsigned i=0; i<A.size(); i++) if (A[i]==value) return i;
            return -1;
        }
        unsigned high = (unsigned)A.size() - 1, low=0;
        while (high+1 >= low+1)     {            // do a binary search
            unsigned mid = (low+high)>>1;
            if (A[mid] > value)            high = mid-1;
            else if (A[mid] < value)    low  = mid+1;
            else return mid;
        }
        return -1;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    //! Outputs a stl container to a string, adds an eol each time after line_length is exceeded
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> std::string container_to_string(const T &c, unsigned line_length=~0)
    {
        if (c.empty()) return "{}";
        typename T::const_iterator it=c.begin();
        std::string str, line("{" + to_string(*it));
        for (++it; it!=c.end(); ++it) {
            std::string item = to_string(*it);
            if (line.size() + item.size() > line_length-1) {
                str += line + ((++it == c.end()) ? "" : ",\n");
                line.clear();
            }
            else line += (line.size() ? "," : "") + item;
        }
        str += line + '}';
        return str;
    }
}

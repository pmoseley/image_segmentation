#include "LevelSet_FMM.h"

namespace matcad {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the initial level set.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_FMM::initialize(Vector& (*ls)(Vector&,double), double s_min, double s_max, unsigned pts) {
        std::vector<FMMRegion> status(_ls.size, DISTANT);
        const double ds = (s_max-s_min)/pts;
        _ls_eqn = ls;

        Matrix discrete_LS = _discretize_LS(ds, s_min, pts);
        Vector gridpt(_NDIM), lspt(_NDIM), temp(_NDIM), normal;
        //TODO: hackish, and could use a lot less storage with a smarter data structure.
        Vector dists(_grid.rows(),true), lspts(_grid.rows());

        // Calculate phi for nodes closest to interface, add to accepted.
        for(unsigned i=0; i<discrete_LS.rows(); i++){
            double s = i*ds + s_min;
            _ls_eqn(lspt,s);
            // Find nodes next to this LS point.
            vectorU32 nodes = nodes_containing_pt(lspt);
            // Find the closest discretized points to these nodes.
            for(vectorU32::iterator n=nodes.begin(); n!=nodes.end(); n++){
                status[*n] = ACCEPTED;
                double dist2 = 0.0;
                for(unsigned i=0; i<_grid.cols(); i++) dist2 += square(_grid(*n,i)-lspt(i));
                if(dists(*n) == 0.0 || dist2 < dists(*n)){
                    dists(*n) = dist2;
                    lspts(*n) = s;
                }
            }
        }
        double h = 0.1*ds;  // TODO, this may need to change, depends on how we find nearest nodes.
        for(unsigned i=0; i<dists.size(); i++){
            if(status[i] == ACCEPTED){
                // Explicitly calculate phi.
                gridpt = _grid.row(i);
                std::pair<double,double> phi = _min_dist(gridpt, lspt, lspts(i), h);
                normal = cross(_ls_eqn(temp,phi.second+ds)-lspt, gridpt-lspt);
                _ls.phi(i) = phi.first/_F(lspt) * (normal(2) > 0.0 ? -1.0 : 1.0);
                vectorU32 nbrs = _conn[i];
                // If the LS runs through a node, make sure neighbors are all ACCEPTED.
                // if(phi.first == 0.0){
                if(phi.first < 0.001){      // TODO
                    for(unsigned j=0; j<nbrs.size(); j++){
                        if(status[nbrs[j]] == ACCEPTED) continue;
                        status[nbrs[j]] = ACCEPTED;
                        gridpt = _grid.row(nbrs[j]);
                        normal = cross(_ls_eqn(temp,phi.second+ds)-lspt, gridpt-lspt);
                        double dist = gridpt.distance_to(lspt);
                        _ls.phi(nbrs[j]) = dist/_F(lspt) * (normal(2) > 0.0 ? -1.0 : 1.0);
                        _mark_trial(_conn[nbrs[j]], status);    // Add neighbors to trial.
                    }
                }
                _mark_trial(nbrs, status);    // Add neighbors to trial.
            }
        }

        // March the distances outwards.
        std::cout<<"Calculated initial distances, marching.\n";
        FastMarching FMM(*this, status);
        FMM.march();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Extend the velocity across the whole domain.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_FMM::velocity_extension() {
        std::vector<FMMRegion> status(_ls.size, DISTANT);
        Vector gridpt, lspt;
        for(unsigned i=0; i<_ls.size; i++){
            // Detect location of the interface by a sign difference.
            vectorU32 &nbrs = _conn[i];
            for(unsigned j=0; j<nbrs.size(); j++){
                if(_ls.phi(i)*_ls.phi(nbrs[j]) <= 0.0){
                    status[i] = ACCEPTED;
                    gridpt = _grid.row(i);
                    // TODO: in the real world, this would be F at the closest point on the interface.
                    // std::pair<double,double> phi = _min_dist(gridpt, lspt, s, h);
                    _ls.vel(i) = _F(gridpt);
                    status[nbrs[j]] = ACCEPTED;
                    gridpt = _grid.row(nbrs[j]);
                    // TODO: in the real world, this would be F at the closest point on the interface.
                    // std::pair<double,double> phi = _min_dist(gridpt, lspt, s, h);
                    _ls.vel(nbrs[j]) = _F(gridpt);
                }
            }
        }

        // Create the initial trial list.
        for(unsigned i=0; i<status.size(); i++)
            if(status[i] == ACCEPTED) _mark_trial(_conn[i], status);

        // March the distances outwards.
        std::cout<<"Calculated initial velocities, marching.\n";
        FastMarching FMM(*this, status);
        FMM.march(true, true);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Reinitialize the SDF.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void LevelSet_FMM::reinitialize() {
        std::vector<FMMRegion> status(_ls.size, DISTANT);

        // TODO: subgrid resolution. See course notes, page 23.
        for(unsigned i=0; i<_ls.size; i++){
            // Detect location of the interface by a sign difference.
            vectorU32 &nbrs = _conn[i];
            for(unsigned j=0; j<nbrs.size(); j++){
                if(_ls.phi(i)*_ls.phi(nbrs[j]) <= 0.0){
                    status[i] = ACCEPTED;
                    status[nbrs[j]] = ACCEPTED;
                }
            }
        }

        // Create the initial trial list.
        for(unsigned i=0; i<status.size(); i++)
            if(status[i] == ACCEPTED) _mark_trial(_conn[i], status);

        // March the distances outwards.
        std::cout<<"Calculated initial distances, marching.\n";
        FastMarching FMM(*this, status);
        FMM.march();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Nodes of the element containing a point.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    vectorU32 LevelSet_FMM::nodes_containing_pt(Vector &lspt) {
        //TODO this is a hack for square grids.  In our code we can use element_containing_pt.
        double dx = fabs(_grid(0,1) - _grid(1,1));
        double dy = fabs(_grid(0,1) - _grid(1,1));
        double x_min(std::numeric_limits<double>::max());
        double y_min(x_min);
        for(unsigned z=0; z<_grid.rows(); z++){
            x_min = min(x_min, _grid(z,0));
            y_min = min(y_min, _grid(z,1));
        }
        unsigned node = sqrt(_grid.rows())*floor((lspt(0)-x_min)/dx) + floor((lspt(1)-y_min)/dy);
        // TODO: in our code use the nodes of the element.
        vectorU32 nodes;
        nodes.push_back(node);
        nodes.push_back(node+1);
        nodes.push_back(node+1+sqrt(_grid.rows()));
        nodes.push_back(node+sqrt(_grid.rows()));
        return nodes;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Add the neighbors of a point to the trial list.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    inline void LevelSet_FMM::_mark_trial(vectorU32 &nbrs, std::vector<FMMRegion> &status) {
        for(unsigned i=0; i<nbrs.size(); i++)
            if(status[nbrs[i]] == DISTANT) status[nbrs[i]] = TRIAL;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Fast Marching.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void FastMarching::march(bool sdf, bool vel_ext) {
        if(!sdf) error("Not implemented yet. Use a signed distance function.");
        _vel_ext = vel_ext;
        // Initialize the trial list.
        for(unsigned i=0; i<_status.size(); i++) if(_status[i] == TRIAL) _trial_pts.push(i);

        while(_trial_pts.size() != 0){
            // Calculate new phi values for all the trial points.
            for(vectorU32::iterator i=_trial_pts.begin(); i!=_trial_pts.end(); i++){
                if(_vel_ext) _vel(*i) = _calc_trial_vel(*i);
                else        _phi(*i) = _calc_trial_phi(*i);
            }
            // Resort, phis have changed.
            _trial_pts.resort();
            // Accept the minimum point.
            _accept_min_point();
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Accept the minimum point from the heap.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void FastMarching::_accept_min_point() {
        const unsigned min_point(*_trial_pts.begin());
        _status[min_point] = ACCEPTED;
        _add_trial_pts(min_point);
        _trial_pts.pop();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Mark new points as trial points.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    void FastMarching::_add_trial_pts(unsigned pt) {
        vectorU32 &nbrs = _conn[pt];
        const short int sign = (_phi(pt) < 0.0) ? -1.0 : 1.0;
        const static double dmax(std::numeric_limits<double>::max());
        for(unsigned i=0; i<nbrs.size(); i++){
            if(_status[nbrs[i]] == DISTANT){
                // Set the new phis very large to ensure proper sorting in the heap.
                if(!_vel_ext) _phi(nbrs[i]) = dmax * sign;
                _status[nbrs[i]] = TRIAL;
                _trial_pts.push(nbrs[i]);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the new phi based on the 4 stencils.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    double FastMarching::_calc_trial_phi(unsigned pt) {
        // TODO: only works for rectangular 2d grids.
        vectorU32 &nbrs = _conn[pt];
        double dx, dx2, dy, dy2, dphi2, phi, S(std::numeric_limits<double>::max());
        for(unsigned i=0; i<nbrs.size(); i++) if(_phi(nbrs[i]) < 0.0){ S *= -1.0; break; }
        Vector row = _grid.row(pt);
        double F = LS._F(row);  // TODO: use LS._ls.vel instead.
        double F2 = F*F;

        for(unsigned i=0; i<nbrs.size(); i++){
            // Define the first neighbor to be in x.
            if(_status[nbrs[i]] == DISTANT || _grid(nbrs[i],1)-_grid(pt,1) != 0.0) continue;
            dx = fabs(_grid(nbrs[i],0)-_grid(pt,0));
            dx2 = dx*dx;
            for(unsigned j=i+1; j<nbrs.size(); j++){
                // Define the second neighbor to be in y.
                if(_status[nbrs[j]] == DISTANT || _grid(nbrs[j],0)-_grid(pt,0) != 0.0) continue;
                dy = fabs(_grid(nbrs[j],1)-_grid(pt,1));
                dy2 = dy*dy;
                dphi2 = square(_phi(nbrs[i])-_phi(nbrs[j]));
                double root = dx2*dy2*F2*(dx2+dy2-F2*dphi2);
                if(root < 0.0){
                    if(S < 0.0) S = max(S, min(_phi(nbrs[i])+dx/F, _phi(nbrs[j])+dy/F));
                    else        S = min(S, min(_phi(nbrs[i])+dx/F, _phi(nbrs[j])+dy/F));
                }else{
                    root = sqrt(root);
                    double denom = 1.0/((dx2+dy2)*F2);
                    phi = F2*(dx2*_phi(nbrs[j]) + dy2*_phi(nbrs[i]));
                    if(S < 0.0) S = max(S, (phi-root)*denom);
                    else        S = min(S, (phi+root)*denom);
                }
            }
        }
        return S;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Calculate the new velocity based on the 4 stencils.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    double FastMarching::_calc_trial_vel(unsigned pt) {
        // TODO: only works for rectangular 2d grids.
        double dx, dy, dphi_i, dphi_j, v;
        vectorU32 &nbrs = _conn[pt];

        for(unsigned i=0; i<nbrs.size(); i++){
            // Define the first neighbor to be in x.
            dx = _grid(nbrs[i],0)-_grid(pt,0);
            dphi_i = (fabs(_phi(nbrs[i])) - fabs(_phi(pt))) / fabs(dx);
            // dphi_i = fabs(_phi(nbrs[i])) - fabs(_phi(pt));
            // dx = square(_grid(nbrs[i],0)-_grid(pt,0));
            if(_status[nbrs[i]] == DISTANT || dx == 0.0 || dphi_i > 0.0) continue;
            for(unsigned j=i+1; j<nbrs.size(); j++){
                // Define the second neighbor to be in y.
                dy = _grid(nbrs[j],1)-_grid(pt,1);
                dphi_j = (fabs(_phi(nbrs[j])) - fabs(_phi(pt))) / fabs(dy);
                // dy = square(_grid(nbrs[j],1)-_grid(pt,1));
                // dphi_j = fabs(_phi(nbrs[j])) - fabs(_phi(pt));
                if(_status[nbrs[i]] == DISTANT || dy == 0.0 || dphi_j > 0.0) continue;
                v = (_vel(nbrs[i])*dphi_i + _vel(nbrs[j])*dphi_j) / (dphi_i + dphi_j);
                // v = (_vel(nbrs[i])*dphi_i*dy + _vel(nbrs[j])*dphi_j*dx)/(dy*dphi_i + dy*dphi_j);
            }
        }
        return v;
    }
}

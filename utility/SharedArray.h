#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <list>
#include <set>
#include "utility.h"
#include "../io/Logger.h"

#ifdef USE_MKL
#define BLAS_ENABLED 1
#include <mkl.h>

#elif defined(USE_ATLAS)
#define BLAS_ENABLED 1
extern "C" {
#include<cblas.h>
#include<clapack.h> }

#else
#define BLAS_ENABLED 0
#endif

using std::string;
using utility::error;
using utility::warning;

// whether or not vector/matrix indices are bounds checked
#ifdef  INDEX_CHECK
#define INDEX_CHECK 1
#else
#define INDEX_CHECK 0
#endif

// whether or not the data size of matrices/vectors is tracked
#ifdef  TRACK_ALLOCATION
#define TRACK_ALLOCATION 1
#else
#define TRACK_ALLOCATION 0
#endif

typedef double Real;
typedef std::vector<unsigned> vectorU32;
typedef std::list<unsigned>   listU32;
typedef std::set<unsigned>    setU32;

namespace matcad {
    // forward declarations
    class Matrix;
    class Vector;

    //! The SharedArray class allows for lazy copy optimization of vectors and matrices each
    //! vector or matrix will have a pointer a SharedArray class.  Copying vectors or matrices
    //! only copies the pointer to this class.  If you change the data in a shared class, a
    //! unique copy must be made.  Any class that contains a SharedArray object, must call
    //! SharedArray::unique() prior to modifying any data
    template<class R> class SharedArray
    {
        int _reference_count;    //! number of objects that share this data
        unsigned _length;        //! number of elements in data
        R *_data;                //! array of data elements

        //! do not allow this class to be assigned
        SharedArray& operator=(const SharedArray& c);
    public:
        //! constructor - default size is none
        SharedArray(unsigned n=0);
        //! copy constructor, makes a unique copy
        SharedArray(const SharedArray &c);
        //! Constructor - makes a copy from a pointers to the beginning and past the last.
        template<class P> SharedArray(const P begin, const P end);
        //! destructor - deletes the shared data
        ~SharedArray();

        //! returns the size of the data
        unsigned size() const;
        //! Returns the number of refernces to this data.
        unsigned reference_count() const;
        //! returns a reference to unique data, makes a copy if needed
        SharedArray* unique();
        //! copies a sequential range from this array to another
        void copy_chunk(SharedArray<R> &dest, unsigned src_loc, unsigned dest_loc, unsigned n);
        //! adds a reference to this data
        int add_reference();
        //! removes a reference to this data, if this was the last one, then deletes data
        static int delete_reference(SharedArray *r);

        //! Returns iterators (pointers) to the beginning and end of the array.
        //@{
        //! Returns a pointer to the beginning of the array.
        R* begin();
        //! Returns a const pointer to the beginning of the array.
        const R* begin() const;
        //! Returns a pointer to the end of the array.
        R* end();
        //! Returns a const pointer to the end of the array.
        const R* end() const;
        //@}

        //! indexing operator - returns a reference to data element
        R& operator[](const unsigned i);
        //! indexing operator - returns a copy of data element
        R  operator[](const unsigned i) const;
        //! indexing operator - returns a reference to data element
        R& at(const unsigned i);
        //! indexing operator - returns a copy of data element
        R  at(const unsigned i) const;

        //! assigns all elements to a constant value
        void assign(const R &v);

        //! adds an array to this one with scaling factor R
        void add_scaled(const SharedArray<R> &r, R s);
        //! Adds an array to this one.
        void add(const SharedArray<R> &r);
        //! Subtracts an array to this one.
        void subtract(const SharedArray<R> &r);
        //! Adds a constant to each element.
        void add(R v);
        //! Subtracts a constant from each element.
        void subtract(R v);
        //! scales this array by a factor s
        void scale(const R &s);

        //! allows determinate to read matrix data
        friend double det(const Matrix &A);
        //! allows inverse function to read matrix data
        friend Matrix inv(const Matrix &A, double *detA);
        //! allows dot product to read matrix data
        friend Real dot(const Vector &x, const Vector &y);
        //! returns the matrix vector product: A*x
        friend Vector operator*(const Matrix &A, const Vector &x);
        //! returns the tranpose-matrix vector product: A'x
        friend Vector operator*(const Vector &x, const Matrix &A);
        //! returns the matrix-matrix product: A*B
        friend Matrix operator*(const Matrix &A, const Matrix &B);
        //! Computes a hash of a Vector.
        friend unsigned hash(const Vector &x);
        //! Computes a hash of a Matrix.
        friend unsigned hash(const Matrix &x);
    };

    //! function that keeps track of amount of data used in matrices and vectors
    int array_storage_count(int change);
}
#include "SharedArray-inl.h"

#pragma once

namespace matcad
{
    // Returns the square of a value
    template<class T> T square(const T& t) { return t*t; }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Constructor - default size is none.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline SharedArray<R>::SharedArray(unsigned n)
      : _reference_count(1), _length(n), _data(_length ? new R[_length] : NULL)
    {
        if (TRACK_ALLOCATION) array_storage_count(sizeof(R)*_length);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Copy constructor, makes a unique copy.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline SharedArray<R>::SharedArray(const SharedArray &c)
      : _reference_count(1), _length(c._length), _data(_length ? new R[_length] : NULL)
    {
        std::copy(c._data, c._data + _length, _data);
        if (TRACK_ALLOCATION) array_storage_count(sizeof(R)*_length);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Constructor that copies from pointers to data (end points to begin + length).
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> template<class P>
    inline SharedArray<R>::SharedArray(const P first, const P end)
      : _reference_count(1), _length(unsigned(end-first)), _data(_length ? new R[_length] : NULL)
    {
        std::copy(first, end, _data);
        if (TRACK_ALLOCATION) array_storage_count(sizeof(R)*_length);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Destructor - deletes the shared data.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline SharedArray<R>::~SharedArray()
    {
        delete _data;
        if (TRACK_ALLOCATION) array_storage_count(-int(_length));
    }
    // Returns the size of the data
    template<class R> inline unsigned SharedArray<R>::size() const { return _length; }
    // Returns the number of refernces to this data.
    template<class R> unsigned SharedArray<R>::reference_count() const { return _reference_count; }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Returns a reference to unique data, makes a copy if needed
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline SharedArray<R>* SharedArray<R>::unique()
    {
        if (_reference_count == 1) return this;    // this array data already is unique
        --_reference_count;                        // remove this reference since it will be copied
        return new SharedArray(*this);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // copies a sequential range from this array to another
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline void SharedArray<R>::copy_chunk(SharedArray<R> &dest,//! destination array
                                           unsigned src_loc,    //! index to read data from
                                           unsigned dest_loc,    //! index to write data to
                                           unsigned n)            //! # of elements to copy
    {
        if (INDEX_CHECK && src_loc+n > size()) error("copy_chunk size too big for source");
        if (INDEX_CHECK && dest_loc+n > dest.size()) error("copy_chunk size too big for dest");
        std::copy(_data+src_loc, _data+src_loc+n, dest._data+dest_loc);
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // adds a reference to this data
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline int SharedArray<R>::add_reference()
    {
        return ++_reference_count;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Removes a reference to this data, if this was the last one, then deletes data
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R>
    inline int SharedArray<R>::delete_reference(SharedArray *r)
    {
        if (!r) return -1;
        if (r->_reference_count > 1) return --(r->_reference_count);
        delete r;
        return 0;
    }
    // Returns a pointer to the beginning of the array.
    template<class R> inline R* SharedArray<R>::begin() { return _data; }
    // Returns a const pointer to the beginning of the array.
    template<class R> inline const R* SharedArray<R>::begin() const { return _data; }
    // Returns a pointer to the end of the array.
    template<class R> inline R* SharedArray<R>::end() { return _data+_length; }
    // Returns a const pointer to the end of the array.
    template<class R> inline const R* SharedArray<R>::end() const { return _data+_length; }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Indexing operator - returns a reference to data element
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline R& SharedArray<R>::operator[](const unsigned i)
    {
        return _data[i];        // do index checking in parent object
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Indexing operator - returns a copy of data element
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline R SharedArray<R>::operator[](const unsigned i) const
    {
        return _data[i];        // do index checking in parent object
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Indexing function - returns a reference to data element
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline R& SharedArray<R>::at(const unsigned i)
    {
        return _data[i];        // do index checking in parent object
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Indexing function - returns a copy of data element
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline R SharedArray<R>::at(const unsigned i) const
    {
        return _data[i];        // do index checking in parent object
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Assigns all values of the array to a constant value
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::assign(const R &v) {
        for (unsigned i=0; i<_length; i++) _data[i] = v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Adds an array to this one with scaling factor R (General implimentation)
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::add_scaled(const SharedArray<R> &r, R s) {
        if (INDEX_CHECK && r.size() != size()) error("add_scaled, arrays must be same size");
        for (unsigned i=0; i<size(); i++) _data[i] += s * r[i];
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Adds an array to this one.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::add(const SharedArray<R> &r) {
        if (INDEX_CHECK && r.size() != size()) error("add, arrays must be same size");
        for (unsigned i=0; i<size(); i++) _data[i] += r[i];
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Subtracts an array to this one.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::subtract(const SharedArray<R> &r) {
        if (INDEX_CHECK && r.size() != size()) error("subtract, arrays must be same size");
        for (unsigned i=0; i<size(); i++) _data[i] -= r[i];
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Adds a constant to each element.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::add(R v) {
        for (R* x=begin(); x!=end(); ++x) *x += v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Subtracts a constant from each element.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::subtract(R v) {
        for (R* x=begin(); x!=end(); ++x) *x -= v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Scales this array by a factor s
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class R> inline void SharedArray<R>::scale(const R &s)    {
        for (R* x=begin(); x<end(); ++x) *x *= s;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Adds an array to this one with scaling factor R (DBL implimentation)
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<>
    inline void SharedArray<double>::add_scaled(const SharedArray<double> &r, double s)
    {
        if (INDEX_CHECK && r.size() != size()) error("add_scaled, arrays must be same size");
        static const int stride=1;
        int nz=(int)size();
        switch (nz) {
            case 6:  _data[5]+=s*r[5];
            case 5:  _data[4]+=s*r[4];
            case 4:  _data[3]+=s*r[3];
            case 3:  _data[2]+=s*r[2];
            case 2:  _data[1]+=s*r[1];
            case 1:  _data[0]+=s*r[0];
            case 0:   break;
            default:  daxpy(&nz, &s, r._data, &stride, _data, &stride);
        }
    }
}

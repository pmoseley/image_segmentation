#pragma once
#include "LevelSet.h"
#include "Helmholtz_Solver.h"

#define PI 3.1415927
namespace matcad {
    using namespace pde_solvers;
    using namespace std;

    class LevelSet_PDE : public LevelSet {
        struct MinLSpts{
            MinLSpts(unsigned i, unsigned j) : s(i), ls(i,j) {}
            Vector s;           //! Values of s corresponding to the min distance for each point.
            Matrix ls;          //! Level set equation evaluated at each s.
        };
    public:
        LevelSet_PDE(Matrix &grid, vvectorU32 &conn) : LevelSet(grid, conn) {};

        //! Calculate the initial level set.
        void initialize(Vector& (*ls)(Vector&,double), double s_min, double s_max, unsigned pts);
        //! Extend the velocity across the whole domain.
        void velocity_extension();
        //! Reinitialize the SDF.
        void reinitialize();
        //! Segment an image.
        void segment_image(Vector &g, Region &bounds, bool implicit, unsigned steps,
                double lambda, double nu, double dt=5.0, double mu=0.05);
    private:
        //! Calculate the distances for the SDF.
        void _calc_distances(double ds, double s_min, double pts, MinLSpts &min_ls_pts);
        //! Calculate the signs for the SDF.
        void _calc_signs(double ds, MinLSpts &min_ls_pts);
        //! Smoothed Dirac function d_epsilon
        inline double _dirac_e(double x, double epsilon=1.5);
    };

    // Smoothed Dirac function d_epsilon
    inline double LevelSet_PDE::_dirac_e(const double x, const double epsilon){
        if(fabs(x) > epsilon) return 0.0;
        else{
            double inv_eps = 1.0/epsilon;
            return 0.5*inv_eps * (1.0+cos(PI*x*inv_eps));
        }
    }
}

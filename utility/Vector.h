#pragma once
#include "SharedArray.h"

#define DATA(i) (*_data)[(i)]
#define FORi for(unsigned i=0; i<size(); i++)

namespace matcad
{
    class Matrix;   //! forward definition of Matrix class
    class Vector    //! basic vector class
    {
        //! Flat shareable array that stores matrix elements.
        SharedArray<Real> *_data;

        //! Makes sure this Vector's data is not shared.
        void _unique();
    public:
        //! Constructor: (defaults: empty Vector, do not prezero elements).
        Vector(unsigned n=0, bool zero=false);
        //! Constructor: constructs a vector of 2 elements.
        Vector(Real x, Real y);
        //! Constructor: constructs a vector of 3 elements.
        Vector(Real x, Real y, Real z);
        //! Constructor: constructs a vector of 6 elements.
        Vector(Real xx, Real yy, Real zz, Real yz, Real xz, Real xy);
        //! Copy constructor from a matrix, appends matrix columns.
        Vector(const Matrix &m);

        //! Copy constructor: adds self to shared data references.
        Vector(const Vector &c);
        //! Destructor: removes reference to shared data.
        ~Vector();

        //! Assigns all elements to a value and (optionally) resizes.
        Vector& assign(double v, unsigned length=~0);
        //! Changes the length of the vector.
        Vector& resize(unsigned length, bool copy=false);
        //! sets all components to zero, optionally resizes the vector
        Vector& zero(unsigned length=~0);
        //! Returns the number of elements in the vector.
        unsigned size() const;
        //! Returns true if the vector has no elements
        bool empty() const;
        //! returns true if the argument vector is the same size as this
        bool same_size(const Vector &r) const;
        //! Returns Euclid norm squared for the difference between this and Vector b.
        Real distance_squared(const Vector &b) const;
        //! Returns Euclid norm for the difference between this and Vector b.
        Real distance_to(const Vector &b, bool squared=false) const;

        //! Assignment operator.
        Vector& operator=(const Vector &c);
        //! Scales each element by corresponding element in Vector s.
        Vector& operator*=(const Vector &s);
        //! Add to this vector.
        Vector& operator+=(const Vector &r);
        //! Subtracts a vector from this vector.
        Vector& operator-=(const Vector &r);
        //! Add a single value to each element.
        Vector& operator+=(Real r);
        //! Subtract a single value from each element.
        Vector& operator-=(Real r);
        //! Adds another vector to this one with a scaling factor.
        Vector& add_scaled(const Vector &r, const Real &s);

        //! returns a copy of the vector element, i
        Real  operator()(const unsigned i) const;
        //! returns a reference to the vector element, i
        Real& operator()(const unsigned i);
        //! Indexes and returns a group of elements.
        Vector operator()(const vectorU32 &I) const;
        //! writes the vector to binary format (from filename)
        unsigned binary_write(const string &filename, bool append=0) const;
        //! writes the vector to binary format (from filestream)
        unsigned binary_write(std::fstream &fid) const;
        //! reads the vector from binary format (from filestream)
        int binary_read(std::fstream &fid);
        //! reads the vector from binary format (from filename)
        int binary_read(const string &name);

        //! Assigns this vector to a one element vector.
        void set(Real x);
        //! assigns this vector to a two element vector
        void set(Real x, Real y);
        //! assigns this vector to a three element vector
        void set(Real x, Real y, Real z);
        //! Assigns this vector to a six element vector.
        void set(Real a, Real b, Real c, Real d, Real e, Real f);
        //! swaps data with vector b
        void swap(Vector &b);

        //! Returns iterators (pointers) to the beginning and end of the Vector.
        //@{
        //! Returns a pointer to the beginning of the Vector.
        Real* begin();
        //! Returns a const pointer to the beginning of the Vector.
        const Real* begin() const;
        //! Returns a pointer to the end of the Vector.
        Real* end();
        //! Returns a const pointer to the end of the Vector.
        const Real* end() const;
        //@}

        //! outputs the vector as a string
        string to_string(const string &delimiter="\t", const string &eol="") const;

        // list of this Vector's friends
        //! returns the matrix vector product: A*x
        friend Vector operator*(const Matrix &A, const Vector &x);
        //! returns the tranpose-matrix vector product: A'x
        friend Vector operator*(const Vector &x, const Matrix &A);
        //! outputs the vector to a stream
        friend std::ostream& operator<<(std::ostream& os, const Vector& v);
        //! Allows Vectors to be converted directly to matrices.
        friend void convert_vector_to_matrix(const Vector &v, Matrix &m);
        //! Allows matrices to be converted directly to vectors.
        friend void convert_matrix_to_vector(const Matrix &m, Vector &v);
        //! Allows operator *= to access Vector private data.
        template<class T> friend T& operator*=(T &x, const Real &s);
        //! Returns the maximum absolute value of the vector.
        template<class T> friend Real max_abs(const T &a);
        //! Returns the minimum absolute value of the vector.
        template<class T> friend Real min_abs(const T &a);
        //! Returns the maximum value of the vector.
        template<class T> friend Real max(const T &a);
        //! Returns the minimum value of the vector.
        template<class T> friend Real min(const T &a);
        //! Returns the dot product of two vectors.
        friend Real dot(const Vector &x, const Vector &y);
        //! Returns the Euclidian norm of a vector
        friend Real norm(const Vector &x);
    };
    //! Creates a random (0,1) vector.
    Vector rand(unsigned length);
    // Forward definitions for subtraction and addition operators.
    Vector operator+(const Vector &a, const Vector &b);
    Vector operator-(const Vector &a, const Vector &b);

    // Adds a value to all elements in a vector.
    Vector operator+(const Vector &a, Real x);
    Vector operator+(Real x, const Vector &a);
    // Subtracts a value from all elements in a vector.
    Vector operator-(const Vector &a, Real x);
    Vector operator-(Real x, const Vector &a);

    //! Returns the cross product of x and y.
    Vector cross(const Vector &x, const Vector &y);
}
#include "Vector-inl.h"
#include "Vector-Matrix-inl.h"
#undef FORi
#undef DATA

#pragma once
#include <cmath>
#include "mkl_poisson.h"
#include "../utility/Region.h"
#include "../utility/Matrix.h"
#include "../utility/Vector.h"
#include "../utility/StringManip.h"

using namespace matcad;

namespace pde_solvers{
    class Helmholtz_Solver{
    public:
        Helmholtz_Solver();
        ~Helmholtz_Solver();

        //! Initialize the Helmholtz equation solver.
        void init(const Region &bounds, int nx, int ny, string &bcs, double coeff);
        //! Solve the Helmholtz equation.
        Vector solve(Vector &f, Vector &bd_ax, Vector &bd_bx, Vector &bd_ay, Vector &bd_by);
        //! Solve the Helmholtz equation, bcs are const.
        Vector solve(Vector &f, double bd_ax, double bd_bx, double bd_ay, double bd_by);
    private:
        //! Allocate memory.
        void _allocate_mem(int nx, int ny);
        //! Convert matcad::Vector to double*
        double* _Vector_to_double(Vector &vec);
        //! Convert double* to matcad::Vector
        Vector _double_to_Vector(double* array);

        DFTI_DESCRIPTOR_HANDLE _xhandle;
        MKL_INT _nx, _ny;
        //! f is the RHS, solution is stored in f after calling MKL.
        double *_dpar, *_f, *_bd_ax, *_bd_bx, *_bd_ay, *_bd_by;
        MKL_INT _ipar[128], _stat;
    };
}

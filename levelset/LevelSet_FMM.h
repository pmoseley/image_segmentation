#pragma once
#include "LevelSet.h"
#include <algorithm>

namespace matcad {
    enum FMMRegion {ACCEPTED, TRIAL, DISTANT};

    class LevelSet_FMM : public LevelSet {
    public:
        LevelSet_FMM(Matrix &grid, vvectorU32 &conn) : LevelSet(grid, conn) {}
        //! Calculate the initial level set.
        void initialize(Vector& (*ls)(Vector&,double), double s_min, double s_max, unsigned pts);
        //! Extend the velocity across the whole domain.
        void velocity_extension();
        //! Reinitialize the SDF.
        void reinitialize();
    private:
        //! Nodes of the element containing a point.
        vectorU32 nodes_containing_pt(Vector &lspt);
        //! Add the neighbors of a point to the trial list.
        inline void _mark_trial(vectorU32 &nbrs, std::vector<FMMRegion> &status);
    };


    //! Sorting function for a min-heap. Always sort by distances.
    // Integrating this function into FMMHeap slows things down dramatically.
    struct HeapSort{
    public:
        HeapSort(Vector &phi) : _phi(phi) {}
        bool operator()(unsigned p1, unsigned p2) const {return fabs(_phi(p1)) > fabs(_phi(p2)); }
    private:
        Vector &_phi;       //! Reference to the levelset function values, for sorting.
    };

    //! Min-heap wrapper class.
    class FMMHeap{
    public:
        FMMHeap(Vector &phi) : cmp(phi) {}
        //! Sort/resort the heap.
        inline void resort(){ make_heap(_heap.begin(),_heap.end(),cmp); }
        //! Pop the min point.
        inline void pop(){ pop_heap(_heap.begin(),_heap.end(),cmp); _heap.pop_back(); }
        //! Push a new point onto the heap.
        inline void push(unsigned pt) { _heap.push_back(pt); push_heap(_heap.begin(),_heap.end(),cmp); }
        //! Return the size of the heap.
        inline unsigned size(){ return _heap.size(); }
        //! Return an iterator to the beginning of the heap.
        inline vectorU32::iterator begin(){ return _heap.begin(); }
        //! Return an iterator to the end of the heap.
        inline vectorU32::iterator end(){ return _heap.end(); }

    private:
        vectorU32 _heap;    //! Heap of phi values.
        HeapSort cmp;       //! Heap-sort function.
    };


    //! Fast Marching Method functions.
    class FastMarching{
    public:
        FastMarching(LevelSet_FMM &ls, std::vector<FMMRegion> &status)
            : LS(ls), _status(status), _vel(ls._ls.vel), _phi(ls._ls.phi),
              _grid(ls._grid), _conn(ls._conn), _trial_pts(ls._ls.phi), _vel_ext(false) {}
        //! Fast Marching.
        void march(bool sdf=true, bool vel_ext=false);
    private:
        //! Accept the minimum point from the heap.
        void _accept_min_point();
        //! Mark new points as trial points.
        void _add_trial_pts(unsigned pt);
        //! Calculate the new phi based on the 4 stencils.
        double _calc_trial_phi(unsigned pt);
        //! Calculate the new velocity based on the 4 stencils.
        double _calc_trial_vel(unsigned pt);

        LevelSet_FMM &LS;               //! Reference to the levelset. TODO: fix _calc_trial_phi, then get rid of this.
        Vector &_vel;                   //! Reference to the velocities.
        Vector &_phi;                   //! Reference to the phis.
        vvectorU32 &_conn;              //! Reference to the connectivity matrix.
        Matrix &_grid;                  //! Reference to the grid values.
        FMMHeap _trial_pts;             //! Heap of the trial points.
        std::vector<FMMRegion> &_status;   //! Status of each point.
        bool _vel_ext;                  //! Velocity extension.
    };
}

function edge_indicator(image_file)
% Used to convert an image file into an edge indicator function,
% as needed for the level set based image segmentation. Writes
% to a .csv file with the same name.

    img = imread(image_file);           % Read in the image file.
    img = double(img(:,:,1));           % Change it into a double array.
    sigma = 1.5;    % Scale parameter in Gaussian kernel for smoothing.
    G = fspecial('gaussian',15,sigma);  % Gaussian kernel.
    img_smooth = conv2(img,G,'same');   % Smooth image by Gaussian convolution.

    % Edge indicator function.
    [Ix,Iy] = gradient(img_smooth);
    f = Ix.^2.0 + Iy.^2.0;
    g = 1.0./(1.0+f);
    csvwrite(regexprep(image_file,'\.jpg','\.csv'),g);
end


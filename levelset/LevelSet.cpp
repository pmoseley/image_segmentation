#include "LevelSet.h"
#ifdef _MSC_VER
#include <float.h>
#define isfinite(x) (_finite(x))
#endif

namespace matcad {
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Discretize the levelset function.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Matrix LevelSet::_discretize_LS(double ds, double s_min, double pts) {
        Matrix discrete_LS(pts, _NDIM);
        Vector lspt(_NDIM);
        for(unsigned i=0; i<discrete_LS.rows(); i++)
            discrete_LS.set_row(i,_ls_eqn(lspt, ds*i+s_min));
        return discrete_LS;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Find the minimum distance to the interface from a starting point, using Newton's method.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    std::pair<double,double> LevelSet::_min_dist(Vector &gridpt, Vector &lspt, double s, double h) {
        unsigned itr(0);
        double newton;
        do{
            const double dist_l2 = gridpt.distance_squared(_ls_eqn(lspt,s-2*h));
            const double dist_l  = gridpt.distance_squared(_ls_eqn(lspt,s-h));
            const double dist_r  = gridpt.distance_squared(_ls_eqn(lspt,s+h));
            const double dist_r2 = gridpt.distance_squared(_ls_eqn(lspt,s+2*h));
            // Calculate the derivatives.
            newton = 2.0*h*(dist_r-dist_l)/(dist_r2+dist_l2);
            if(!isfinite(newton)) break;
            s -= newton;
        }while(itr++ < 25 && newton < TOL);
        // Update lspt with the newest s.
        _ls_eqn(lspt,s);
        // Calculate the final optimal distance.
        return std::make_pair<double,double>(gridpt.distance_to(lspt),s);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Return the forward and backward derivatives at a point.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Vector LevelSet::_hamiltonian_derivatives(Vector &f, vectorU32 &nbrs, unsigned pt) {
        // TODO: all this only works on a rectangular grid.
        Vector delta(_NDIM);
        Matrix deltas(nbrs.size(), _NDIM);
        for(unsigned i=0; i<nbrs.size(); i++){
            _grid.row_difference(pt,nbrs[i],delta);
            deltas.set_row(i,delta);
        }

        // This only works in 2d, could be extended.
        Vector derivatives(_NDIM*2, true);
        for(unsigned i=0; i<nbrs.size(); i++){
            if(deltas(i,0) > 0.0 && deltas(i,1) == 0.0)
                derivatives(0) = (f(pt) - f(nbrs[i]))/deltas(i,0);
            else if(deltas(i,0) < 0.0 && deltas(i,1) == 0.0)
                derivatives(1) = (f(pt) - f(nbrs[i]))/deltas(i,0);
            else if(deltas(i,0) == 0.0 && deltas(i,1) > 0.0)
                derivatives(2) = (f(pt) - f(nbrs[i]))/deltas(i,1);
            else if(deltas(i,0) == 0.0 && deltas(i,1) < 0.0)
                derivatives(3) = (f(pt) - f(nbrs[i]))/deltas(i,1);
        }
        return derivatives;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Find the spatial deltas between a point and all it's neighbors.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Matrix LevelSet::_find_deltas(unsigned pt) {
        vectorU32 &nbrs = _conn[pt];
        // Calculate the dx, dy, and dz.
        Vector delta(_NDIM);
        Matrix deltas(nbrs.size(), _NDIM);
        for(unsigned i=0; i<nbrs.size(); i++){
            _grid.row_difference(pt,nbrs[i],delta);
            deltas.set_row(i,delta);
            // Hack for periodic meshes.
            for(unsigned j=0; j<_NDIM; j++){
                if(deltas(i,j)>=2.0) deltas(i,j)=-1.0;
                else if(deltas(i,j)<=-2.0) deltas(i,j)=1.0;
            }
        }
        return deltas;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Take a spatial derivative using central differences.
    ////////////////////////////////////////////////////////////////////////////////////////////////
    double LevelSet::_derivative(Vector &f, Matrix &deltas, unsigned pt, unsigned dim, bool second_order) {
        vectorU32 &nbrs = _conn[pt];
        unsigned L,R;
        // Find the proper neighbors.
        for(unsigned i=0; i<deltas.rows(); i++){
            if(deltas(i,dim) == 0.0) continue;
            (deltas(i,dim) > 0.0 ? L : R) = i;
        }

        // Calculate the derivative df.
        if(second_order) return _d2(f(nbrs[L]),f(pt),f(nbrs[R]), deltas(L,dim),deltas(R,dim));
        return _d(f(nbrs[L]),f(nbrs[R]), deltas(L,dim),deltas(R,dim));
        // vectorU32 &nbrsL = _conn[nbrs[L]], &nbrsR = _conn[nbrs[R]];
        // if(dim == 0){
        //     if(second_order)
        //         return (-f(nbrsR[1])+16*f(nbrs[R])-30*f(pt)+16*f(nbrs[R])-f(nbrsL[0]))/(12*deltas(L,dim));
        //     return (-f(nbrsR[1])+8*f(nbrs[R])-8*f(nbrs[L])+f(nbrsL[0]))/(12*deltas(L,dim));
        // }else{
        //     if(second_order)
        //         return (-f(nbrsR[2])+16*f(nbrs[R])-30*f(pt)+16*f(nbrs[R])-f(nbrsL[3]))/(12*deltas(L,dim));
        //     return (-f(nbrsR[2])+8*f(nbrs[R])-8*f(nbrs[L])+f(nbrsL[3]))/(12*deltas(L,dim));
        // }
    }
}

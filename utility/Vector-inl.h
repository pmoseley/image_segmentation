#pragma once
#include <cmath>
namespace matcad
{
    // Vector constructor - optionally prezeros the elements
    inline Vector::Vector(unsigned n, bool zero) : _data(new SharedArray<Real>(n)) {
        if (zero) _data->assign(0.0);
    }

    // Constructor - constructs a vector of 2 elements
    inline Vector::Vector(Real x, Real y) : _data(new SharedArray<Real>(2))    {
        DATA(0) = x;
        DATA(1) = y;
    }

    // Constructor - constructs a vector of 3 elements
    inline Vector::Vector(Real x, Real y, Real z)  : _data(new SharedArray<Real>(3)) {
        DATA(0) = x;
        DATA(1) = y;
        DATA(2) = z;
    }

    // Constructor - constructs a vector of 6 elements.
    inline Vector::Vector(Real xx, Real yy, Real zz, Real yz, Real xz, Real xy)
        : _data(new SharedArray<Real>(6)) {
        DATA(0) = xx;
        DATA(1) = yy;
        DATA(2) = zz;
        DATA(3) = yz;
        DATA(4) = xz;
        DATA(5) = xy;
    }

    // Copy constructor from a matrix, appends matrix columns
    inline Vector::Vector(const Matrix &m) : _data(NULL) {
        convert_matrix_to_vector(m, *this);
    }

    // Vector copy constructor - only copies reference of data
    inline Vector::Vector(const Vector &c) : _data(c._data)    {
        _data->add_reference();
    }

    // Vector destructor - removes this vector's reference from the data
    inline Vector::~Vector() {
        SharedArray<Real>::delete_reference(_data);
    }

    // Assigns all elements to a value and (optionally) resizes.
    inline Vector& Vector::assign(double v, unsigned length) {
        _unique();
        if (~length && length != size()) {
            resize(length);
        }
        _data->assign(v);
        return *this;
    }

    // Returns the size of the vector
    inline unsigned Vector::size() const { return _data->size(); }

    // Returns true if the vector has no elements
    inline bool Vector::empty() const { return _data->size() == 0; }

    // Returns true if the argument vector is the same size as this
    inline bool Vector::same_size(const Vector &r) const { return r.size()==size(); }

    // Assigns a vector to this one (copies the reference only)
    inline Vector& Vector::operator=(const Vector &c) {
        if (this->_data == c._data) return *this; // prevents self-assignment
        SharedArray<Real>::delete_reference(_data);
        _data = c._data;
        _data->add_reference();
        return *this;
    }

    // Scales each element by corresponding element in Vector s.
    inline Vector& Vector::operator*=(const Vector &s) {
        if (INDEX_CHECK && size()!=s.size()) error("unequal sizes in elementwise vector scale.");
        _unique();
        FORi DATA(i) *= (*s._data)[i];
        return *this;
    }

    // Adds a matrix to this one.
    inline Vector& Vector::operator+=(const Vector &r) {
        _unique();
        _data->add(*r._data);
        return *this;
    }

    // Subtracts a matrix from this one.
    inline Vector& Vector::operator-=(const Vector &r) {
        _unique();
        _data->subtract(*r._data);
        return *this;
    }

    // Add a single value to each element.
    inline Vector& Vector::operator+=(Real r) {
        _unique();
        _data->add(r);
        return *this;
    }

    // Subtract a single value from each element.
    inline Vector& Vector::operator-=(Real r) {
        _unique();
        _data->subtract(r);
        return *this;
    }

    // Adds another vector to this one with a scaling factor.
    inline Vector& Vector::add_scaled(const Vector &r, const Real &s) {
        _unique();
        _data->add_scaled(*r._data, s);
        return *this;
    }

    // Returns the square root of a Vector dotted with itself.
    inline Real norm(const Vector &x) {
        return sqrt(dot(x,x));
    }

    // Returns a copy of the vector element, i.
    inline Real Vector::operator()(const unsigned i) const {
        if (INDEX_CHECK && i >= size())    error("Vector index out of bounds");
        return DATA(i);
    }

    // Returns a reference to the vector element, i.
    inline Real& Vector::operator()(const unsigned i) {
        _unique();
        if (INDEX_CHECK && i >= size()) error("Vector index out of bounds");
        return DATA(i);
    }

    // Indexes and returns a group of elements.
    inline Vector Vector::operator()(const vectorU32 &I) const {
        Vector sub((unsigned)I.size());
        for (unsigned i=0; i<sub.size(); i++) sub(i) = (*this)(I[i]);
        return sub;
    }

    // Assigns this vector to a one element vector.
    inline void Vector::set(Real x)    {
        resize(1);
        _unique();
        DATA(0)=x;
    }

    // Assigns this vector to a two element vector.
    inline void Vector::set(Real x, Real y)    {
        resize(2);
        _unique();
        DATA(0)=x;
        DATA(1)=y;
    }

    // Assigns this vector to a three element vector.
    inline void Vector::set(Real x, Real y, Real z) {
        resize(3);
        _unique();
        DATA(0)=x;
        DATA(1)=y;
        DATA(2)=z;
    }

    // Assigns this vector to a six element vector.
    inline void Vector::set(Real a, Real b, Real c, Real d, Real e, Real f) {
        resize(6);
        _unique();
        DATA(0)=a;
        DATA(1)=b;
        DATA(2)=c;
        DATA(3)=d;
        DATA(4)=e;
        DATA(5)=f;
    }

    // Returns Euclid norm squared for the difference between this and Vector b.
    inline Real Vector::distance_squared(const Vector &b) const {
        if (INDEX_CHECK && size()!=b.size()) error("distance_to requires same size vectors.");
         switch (size())  {
        case 0:    return 0.0;
        case 1:    return square(DATA(0)-b(0));
        case 2:    return square(DATA(1)-b(1)) + square(DATA(0)-b(0));
        case 3:    return square(DATA(2)-b(2)) + square(DATA(1)-b(1)) + square(DATA(0)-b(0));
        default:
            Vector r(b-*this);
            return dot(r,r);
        }
    }

    // Returns Euclid norm for the difference between this and Vector b.
    inline Real Vector::distance_to(const Vector &b, bool squared) const {
        if (squared) return distance_squared(b);
        return sqrt(distance_squared(b));
    }

    // Returns the cross product of x and y.
    inline Vector cross(const Vector &x, const Vector &y) {
        if (INDEX_CHECK && !x.same_size(y))
            error("Cross product must have same sized vectors.");
        if (x.size() == 3) {
            return Vector(x(1)*x(2)-x(2)*y(1),
                          x(2)*y(0)-x(0)*y(2),
                          x(0)*y(1)-x(1)*y(0));
        }
        else if (x.size()==2) {
            return Vector(0.0, 0.0, x(0)*y(1)-x(1)*y(0)); // note return a 3D vector
        }
        else error("Cross product only for 2 or 3 element Vectors.");
        return Vector(); // make Intel compiler happy
    }

    // Exchanges pointers to SharedArrays.  Useful for preventing unneccessary destructor calls.
    inline void Vector::swap(Vector &b) { std::swap(_data, b._data); }

    // Returns a pointer to the beginning of the Vector.
    inline Real* Vector::begin() { _unique(); return _data->begin(); }
    // Returns a const pointer to the beginning of the Vector.
    inline const Real* Vector::begin() const { return _data->begin(); }
    // Returns a pointer to the end of the Vector.
    inline Real* Vector::end() { _unique(); return _data->end(); }
    // Returns a const pointer to the end of the Vector.
    inline const Real* Vector::end() const { return _data->end(); }

    // Makes sure this Vector's data is not shared.
    inline void Vector::_unique() { _data = _data->unique(); }

    // returns the sum of matrices A and B
    inline Vector operator+(const Vector &x, const Vector &y) {
        Vector z(x);
        z += y;
        return z;
    }

    // returns the difference of matrices A and B
    inline Vector operator-(const Vector &x, const Vector &y) {
        Vector z(x);
        z -= y;
        return z;
    }

    // Adds a value to all elements in a vector.
    inline Vector operator+(const Vector &a, Real x) {
        Vector b(a);
        b += x;
        return b;
    }

    // Make adding value to element commumative.
    inline Vector operator+(Real x, const Vector &a) { return a + x; }

    // Subtracts a value from all elements in a vector.
    inline Vector operator-(const Vector &a, Real x) {
        Vector b(a);
        b -= x;
        return b;
    }

    // Subtracts a value from all elements in a vector.
    inline Vector operator-(Real x, const Vector &a) {
        Vector b(a.size());
        for(unsigned i=0; i<a.size(); i++) b(i) = x-a(i);
        return b;
    }
}

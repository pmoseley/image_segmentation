#pragma once
namespace matcad
{
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //! scales vectors or matices by a constant
    /*! makes object unique and calls the scaling function of SharedArray */
    template<class T> inline T& operator*=(T &t, const Real &s)
    {
        t._unique();
        t._data->scale(s);
        return t;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! scales a matrix or vector by a constant
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> inline T operator*(const T &A, const Real &x)
    {
        T B(A);
        B*=x;
        return B;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! scales a matrix or vector by a constant (communitive)
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> inline T operator*(Real x, const T &A)
    {
        return A*x;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! returns the maximum absolute value of a vector or matrix
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> Real max_abs(const T &a)
    {
        if (a.empty()) return 0.0;
        Real v = fabs(a._data->at(0));
        for (unsigned i=1; i<a.size(); i++)    v = std::max(fabs(a._data->at(i)), v);
        return v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! returns the minimum absolute value of a vector or matrix
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> Real min_abs(const T &a)
    {
        if (a.empty()) return 0.0;
        Real v = fabs(a._data->at(0));
        for (unsigned i=1; i<a.size(); i++)    v = std::min(fabs(a._data->at(i)), v);
        return v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! returns the maximum value of a vector or matrix
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> Real max(const T &a)
    {
        if (a.empty()) return 0.0;
        Real v = a._data->at(0);
        for (unsigned i=1; i<a.size(); i++)    v = std::max(a._data->at(i), v);
        return v;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //! returns the minimum value of a vector or matrix
    ///////////////////////////////////////////////////////////////////////////////////////////////
    template<class T> Real min(const T &a)
    {
        if (a.empty()) return 0.0;
        Real v = a._data->at(0);
        for (unsigned i=1; i<a.size(); i++)    v = std::min(a._data->at(i), v);
        return v;
    }
}

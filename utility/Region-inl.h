#include "Region.h"

namespace matcad {
    ////////////////////////////////////////////////////////////////////////////////
    // Constructs a rectangular region bounded by two corner points.
    ////////////////////////////////////////////////////////////////////////////////
    inline Region::Region(const Vector &a, const Vector &b) : _min(a), _max(b) {
        if (INDEX_CHECK && a.size()!=b.size())
            error("Region points must have same number of dimensions.");
        _check_min_max();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Returns the number of dimensions.
    ////////////////////////////////////////////////////////////////////////////////
    inline unsigned Region::num_dim() const  { return _min.size(); }

    ////////////////////////////////////////////////////////////////////////////////
    // Returns true if x falls within region with tolerance, tol.
    ////////////////////////////////////////////////////////////////////////////////
    inline bool Region::point_in_region(const Vector &x, double tol) const {
        if (INDEX_CHECK && x.size() != num_dim())
            error("Point and region have different dimensions\n");
        for (unsigned i=0; i<_min.size(); i++) {
            const double TOL = range(i) * tol;
            if (!in_range(_min(i), _max(i), x(i), TOL)) return false;
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Returns the length of the region along a dimension, i.
    ////////////////////////////////////////////////////////////////////////////////
    inline double Region::range(unsigned i) const {
        if (INDEX_CHECK && i>=num_dim())
            error("Region index exceeds num dimensions.");
        return _max(i) - _min(i);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Sets the minimum point along each dimension.
    ////////////////////////////////////////////////////////////////////////////////
    inline void Region::set_min(Vector &a) {
        if (a.size() != num_dim())
            error("New region point has wrong number of dimensions.");
        _min = a;
        _check_min_max();
    }
    ////////////////////////////////////////////////////////////////////////////////
    // Sets the maximum point along each dimension.
    ////////////////////////////////////////////////////////////////////////////////
    inline void Region::set_max(Vector &a) {
         if (a.size() != num_dim())
            error("New region point has wrong number of dimensions.");
        _max = a;
        _check_min_max();
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Enforces that _min(i) < _max(i)
    ////////////////////////////////////////////////////////////////////////////////
    inline void Region::_check_min_max() {
        // Make sure that _min has all minimums.
        for (unsigned i=0; i<num_dim(); i++) {
            if (_min(i) > _max(i)) std::swap(_min(i), _max(i));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Returns true if (x0-TOL) <= x <= (x1+TOL).
    ////////////////////////////////////////////////////////////////////////////////
    template<class T>
    inline bool in_range(const T &x0, const T &x1, const T &x, T TOL) {
        return (x0-TOL)<=x && x<=(x1+TOL);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Find the boundaries of a matrix, uses subset if given indexes.
    ////////////////////////////////////////////////////////////////////////////////
    inline Region find_bounds(const Matrix &x, const vectorU32 &idx) {
        if(x.empty()) return Region(Vector(),Vector());
        Vector min = x.row(0), max = x.row(0);
        auto index=idx.cbegin();
        unsigned i = (idx.empty() ? 0 : *index);
        while(idx.empty() ? i<x.rows():index!=idx.cend()){
            for(unsigned j=0; j<x.cols(); ++j){
                if(x(i,j)<min(j))      min(j) = x(i,j);
                else if(x(i,j)>max(j)) max(j) = x(i,j);
            }
            if(idx.empty()) i++;
            else i=*(index++);
        }
        return Region(min,max);
    }


}


#pragma once
#include <vector>
#include "../utility/Matrix.h"
#include "../utility/Vector.h"
#include "../utility/Region.h"
#include "../utility/StringManip.h"

typedef std::vector<vectorU32> vvectorU32;
namespace matcad {
    struct EdgeIndicator{
        EdgeIndicator(Vector &bmin, Vector &bmax) : bounds(bmin,bmax) {}
        Vector g;           //!< Edge indicator data.
        Matrix grid;        //!< Matrix of (x,y) coordinates.
        vvectorU32 conn;    //!< Connectivity matrix.
        Region bounds;      //!< Boundary of the system.
    };

    //! Read in edge indicator function.
    EdgeIndicator read_csv(const string &fname);
    //! Create a matrix containing the x and y coordinates.
    void meshgrid(EdgeIndicator &ei, bool periodic=false);
    //! Writes out datafile for plotting with Matlab or GnuPlot.
    void write_csv(string filename, Vector &data, bool circular=false);
    //! Writes out datafile for plotting with Matlab or GnuPlot.
    void write_csv(string filename, Matrix &data, bool circular=false);
}

#pragma once
#include <vector>
#include <utility>
#include <set>
#include <limits>
#include <cmath>
#include <mkl.h>
#include "../utility/Matrix.h"
#include "../utility/Vector.h"
#include "../utility/StringManip.h"
#include "../utility/utility.h"
#include "../utility/Region.h"

#define TOL 1e-10
typedef std::vector<unsigned> vectorU32;
typedef std::vector<vectorU32> vvectorU32;
namespace matcad {
    using namespace std;
    using std::min; using std::max;

    struct LSFunc{
        LSFunc(unsigned r) : size(r), phi(r,true), vel(r,true) {}
        unsigned size;      //! Number of points.
        Vector phi;         //! Signed distances or time-of-crossings, depending on method.
        Vector vel;         //! Velocities.
    };

    inline double _eikonal_eqn(Vector &x){ return 1.0; }

    class LevelSet {
    protected:
        LevelSet(Matrix &grid, vvectorU32 &conn)
            : _ls_eqn(NULL), _F(_eikonal_eqn), _grid(grid), _conn(conn), _NDIM(grid.cols()), _ls(grid.rows()) {}
    public:
        virtual ~LevelSet() {}

        //! Calculate the initial level set.
        virtual void initialize(Vector& (*ls)(Vector&,double), double s_min, double s_max, unsigned pts)=0;
        //! Extend the velocity across the whole domain.
        virtual void velocity_extension()=0;
        //! Reinitialize the SDF.
        virtual void reinitialize()=0;
        //! Return the signed distance function.
        inline Vector& phi() { return _ls.phi; }
        //! Return the extensional velocity field.
        inline Vector& velocity() { return _ls.vel; }
        //! Set the signed distance function. Testing only!
        inline void set_phi(Vector &phi) { _ls.phi = phi; _ls.size = phi.size(); }
        //! Set the evolution equation.
        inline void set_F(double (*F)(Vector&)) { _F = F; }

        friend class FastMarching;  //! Allow FastMarching to access private data.
    protected:
        //! Discretize the levelset function.
        Matrix _discretize_LS(double ds, double s_min, double pts);
        //! Find the minimum distance to the interface from a starting point, using Newton's method.
        std::pair<double,double> _min_dist(Vector &gridpt, Vector &lspt, double s, double h);
        //! Return the forward and backward derivatives at a point.
        Vector _hamiltonian_derivatives(Vector &f, vectorU32 &nbrs, unsigned pt);
        //! Find the spatial deltas between a point and all it's neighbors.
        Matrix _find_deltas(unsigned pt);
        //! Take a spatial derivative using central differences.
        double _derivative(Vector &f, Matrix &deltas, unsigned pt, unsigned dim, bool second_order=false);
        //! Take a first derivative using central differences.
        inline double _d(double fL, double fR, double dL, double dR)
            { return (fR-fL)/(fabs(dL)+fabs(dR)); }
        //! Take a second derivative using central differences.
        inline double _d2(double fL, double f, double fR, double dL, double dR)
            { return (fR-2*f+fL)/(fabs(dL*dR)); }
        //! Return an approximation to the sign along the interface.
        inline double _S(double phi, double h)
            { return phi/sqrt(phi*phi + h*h); }
        //! Return an approximation to the sign along the interface. Slightly more stable.
        inline double _S(double phi, double norm_dphi, double h)
            { return phi/sqrt(phi*phi+norm_dphi*norm_dphi*h*h); }

        const unsigned _NDIM;               //! Number of dimensions.
        Matrix &_grid;                      //! Reference to the nodal mesh.
        vvectorU32 &_conn;                  //! Reference to the connectivity.
        Vector& (*_ls_eqn)(Vector&,double); //! Parametric levelset equation.
        LSFunc _ls;                         //! Levelset function phi.
        double (*_F)(Vector&);              //! Evolution equation.
    };
}

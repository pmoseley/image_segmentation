#pragma once
#include "Vector.h"
#include <cmath>
#include <algorithm>

namespace matcad
{
    ////////////////////////////////////////////////////////////////////////////
    // Constructor - (defaults: empty Matrix, do not prezero elements).
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix::Matrix(unsigned nrow, unsigned ncol, bool zero)
      : _t(0), _data(new SharedArray<Real>(nrow*ncol)), _rows(nrow), _cols(ncol)
    {
        if (zero) _data->assign(0.0);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Matrix copy constructor - only copies reference of data and sizes
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix::Matrix(const Matrix &c)
      : _t(c._t), _data(c._data), _rows(c._rows), _cols(c._cols)
    {
        _data->add_reference();
    }
    ////////////////////////////////////////////////////////////////////////////
    // Copy constructor from Vector, defaults to a column matrix.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix::Matrix(const Vector &c, unsigned ncol) : _t(0), _data(NULL)
    {
        convert_vector_to_matrix(c, *this);
        _cols = ncol;
        _rows = _data->size() / _cols;
        if (_data->size() != rows()*cols())
            error("converting Vector to Matrix, bad # of columns");
    }
    ////////////////////////////////////////////////////////////////////////////
    // Copy constructor that copies from iterators.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix::Matrix(const std::vector<Real> &x, unsigned m, unsigned n)
      : _t(0), _rows(m), _cols(n)
    {
        _data = new SharedArray<Real>(x.begin(), x.end());
        if (!~_rows) _rows = size()/cols();
        if (INDEX_CHECK && size() != rows()*cols())
            error("Matrix constructed from iterators, size is not correct");
    }
    ////////////////////////////////////////////////////////////////////////////
    // Vector destructor - removes this vector's reference from the data.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix::~Matrix()
    {
        SharedArray<Real>::delete_reference(_data);
    }
    // Toggles the transpose flag on this Matrix.
    inline Matrix& Matrix::t()  { _t^=1; return *this; }
    // returns the number of rows in the matrix
    inline unsigned Matrix::rows() const { return _t ? _cols : _rows; }
    // returns the number of columns in the matrix
    inline unsigned Matrix::cols() const { return _t ? _rows : _cols; }
    // returns the size of the vector
    inline unsigned Matrix::size() const { return _data->size(); }
    // returns true if the matrix has no elements
    inline bool Matrix::empty() const { return _data->size() == 0; }
    // returns true if the matrix is square
    inline bool Matrix::square() const { return rows() == cols(); }
    ////////////////////////////////////////////////////////////////////////////
    // Checks the matrix size.
    ////////////////////////////////////////////////////////////////////////////
    inline bool Matrix::is_size(unsigned nrow, unsigned ncol) const
    {
        return (rows()==nrow && cols()==ncol);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Sets the difference between two rows.
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::row_difference(unsigned a, unsigned b, Vector &rab) const
    {
        if (INDEX_CHECK && (std::max(a,b)>=rows() || cols()==0))
            error("Matrix:row_difference out of bounds");
        if (rab.size() != cols()) rab.resize(cols());
        switch (cols()) {
            case 3: rab(2) = MATRIX_DATA(a,2)-MATRIX_DATA(b,2);
            case 2: rab(1) = MATRIX_DATA(a,1)-MATRIX_DATA(b,1);
            case 1: rab(0) = MATRIX_DATA(a,0)-MATRIX_DATA(b,0);
            default:
                for (unsigned j=0; j<cols(); ++j) {
                    rab(j) = MATRIX_DATA(a,j)-MATRIX_DATA(b,j);
                }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns a row of the matrix.
    ////////////////////////////////////////////////////////////////////////////
    inline Vector Matrix::row(const unsigned i) const
    {
        Vector x(cols());
        if(INDEX_CHECK && i>=rows()) error("Matrix.row: out of bounds.");
        for (unsigned j=0; j<cols(); j++) x(j) = MATRIX_DATA(i,j);
        return x;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns a column of the matrix.
    ////////////////////////////////////////////////////////////////////////////
    inline Vector Matrix::column(const unsigned j) const
    {
        Vector x(rows());
        if(INDEX_CHECK && j>=cols()) error("Matrix.column: out of bounds.");
        for (unsigned i=0; i<rows(); i++) x(i) = MATRIX_DATA(i,j);
        return x;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Scales each row by the corresponding element in input Vector.
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::scale_rows(const Vector &s)
    {
        if (INDEX_CHECK && s.size()!=rows())
            error("scale_rows, input vector is wrong size");
        _unique();
        FORij MATRIX_DATA(i,j) *= s(i);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Scales each column by the corresponding element in input Vector.
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::scale_columns(const Vector &s)
    {
        if (INDEX_CHECK && s.size()!=cols())
            error("scale_columns, input vector is wrong size");
        _unique();
        FORij MATRIX_DATA(i,j) *= s(j);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Optimized add_to_row adds 3 values explicitly,
    // and uses 4x unrolled loop to add remainder
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::add_to_row(const unsigned i, const Vector &x, double alpha)
    {
        if (INDEX_CHECK && i>=rows())
            error("Matrix::add_to_row invalid row.");
        if (INDEX_CHECK && x.size()!=cols())
            error("Matrix::add_to_row size mismatch.");
        _unique();
        unsigned j = x.size()&3;     // same as x.size()%4
        switch (j) {                 // takes care of first three columns
        case 3: MATRIX_DATA(i,2) += x(2)*alpha;
        case 2: MATRIX_DATA(i,1) += x(1)*alpha;
        case 1: MATRIX_DATA(i,0) += x(0)*alpha;
        default:     // any remaining columns must be divisible by four
            while (j < x.size()) {
                MATRIX_DATA(i,j) += x(j++)*alpha;
                MATRIX_DATA(i,j) += x(j++)*alpha;
                MATRIX_DATA(i,j) += x(j++)*alpha;
                MATRIX_DATA(i,j) += x(j++)*alpha;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Optimized add_to_column adds 3 values explicitly,
    // and uses 4x unrolled loop to add remainder
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::add_to_column(const unsigned j, const Vector &x, double alpha)
    {
        if (INDEX_CHECK && j>=cols())
            error("Matrix::add_to_column invalid column.");
        if (INDEX_CHECK && x.size()!=rows())
            error("Matrix::add_to_column size mismatch.");
        _unique();
        unsigned i = x.size()&3;            // same as x.size()%4
        switch (i) {                        // takes care of first three rows
        case 3: MATRIX_DATA(2,j) += x(2)*alpha;
        case 2: MATRIX_DATA(1,j) += x(1)*alpha;
        case 1: MATRIX_DATA(0,j) += x(0)*alpha;
        default:  // any remaining rows must be divisible by four
            while (i < x.size()) {
                MATRIX_DATA(i,j) += x(i++)*alpha;
                MATRIX_DATA(i,j) += x(i++)*alpha;
                MATRIX_DATA(i,j) += x(i++)*alpha;
                MATRIX_DATA(i,j) += x(i++)*alpha;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    // Sets the matrix row to the vector row
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::set_row(const unsigned i, const Vector &row)
    {
        if (INDEX_CHECK && row.size() != cols())
            error("cannot set row, wrong # of elements");
        if(INDEX_CHECK && i>=rows()) error("set_row out of bounds.");
        _unique();
        for (unsigned j=0; j<cols(); j++) MATRIX_DATA(i,j) = row(j);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Sets the matrix column to the vector col
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::set_column(const unsigned j, const Vector &col)
    {
        if (INDEX_CHECK && col.size() != rows())
            error("cannot set column, wrong # of elements");
        _unique();
        for (unsigned i=0; i<rows(); i++) MATRIX_DATA(i,j) = col(i);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Assigns all elements to zero
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::zero()
    {
        _unique();
        _data->assign(0.0);
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Resizes and assigns all elements to zero
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::zero(unsigned nrow, unsigned ncol)
    {
        resize(nrow, ncol);
        _unique();
        _data->assign(0.0);
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Assigns a vector to this one (copies the reference only)
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::operator=(const Matrix &c)
    {
        if (this == &c) return *this; // prevents self-assignment
        SharedArray<Real>::delete_reference(_data);
        _data = c._data;
        _data->add_reference();
        _rows = c.rows();
        _cols = c.cols();
        _t = c._t;
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns a copy of the vector element: i,j
    ////////////////////////////////////////////////////////////////////////////
    inline Real Matrix::operator()(unsigned i, unsigned j) const
    {
        if (INDEX_CHECK && (i>=rows() || j>=cols()))
            error("Matrix index out of bounds");
        return MATRIX_DATA(i,j);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns a reference to the vector element: i,j
    ////////////////////////////////////////////////////////////////////////////
    inline Real& Matrix::operator()(unsigned i, unsigned j)
    {
        _unique();
        if (INDEX_CHECK && (i>=rows() || j>=cols()))
            error("Matrix index out of bounds");
        return MATRIX_DATA(i,j);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Extracts matrix subset, empty argument returns all indices in dimension.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix Matrix::operator()(const vectorU32 &I, const vectorU32 &J) const
    {
        if (INDEX_CHECK) {
            if (!I.empty() && *std::max_element(I.begin(), I.end())>=rows())
                error("Invalid row index for creating matrix subset.");
            if (!J.empty() && *std::max_element(J.begin(), J.end()) >= cols())
                error("Invalid column index for creating matrix subset.");
        }

        const unsigned sub_rows = I.empty()?rows():(unsigned)I.size();
        const unsigned sub_cols = J.empty()?cols():(unsigned)J.size();
        Matrix sub(sub_rows, sub_cols);
        for (unsigned j=0; j<sub.cols(); j++) {
            const unsigned col_index = J.empty() ? j : J[j];
            for (unsigned i=0; i<sub.rows(); i++) {
                const unsigned row_index = I.empty() ? i : I[i];
                sub(i, j) = MATRIX_DATA(row_index, col_index);
            }
        }
        return sub;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Adds a matrix to this one
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::operator+=(const Matrix &r)
    {
        return add_scaled(r,  1.0);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Subtracts a matrix from this one
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::operator-=(const Matrix &r)
    {
        return add_scaled(r, -1.0);
    }
    ////////////////////////////////////////////////////////////////////////////
    // Adds a scaled matrix to this one, default scaling factor is unity.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::add_scaled(const Matrix &r, const double s)
    {
        _unique();
        if (_t == r._t) _data->add_scaled(*r._data, s);
        else FORji MATRIX_DATA(i,j) += r(i,j)*s;
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Adds subset of scaled matrix to this one, default factor is unity.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::add_scaled(const vectorU32 &I, const Matrix &r,
                                      const double s)
    {
        if (INDEX_CHECK) {
            if (!I.empty() && *std::max_element(I.begin(), I.end())>=rows())
                error("Invalid row index for creating matrix subset.");
        }
        _unique();
        for (unsigned j=0; j<cols(); j++)
            for (unsigned i=0; i<I.size(); i++)
                MATRIX_DATA(I[i],j) += r(I[i],j)*s;
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Scales a matrix by a constant and returns a reference to itself.
    // Makes this matrix unique and calls the scaling function of SharedArray.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix& Matrix::operator*=(const Real &x)
    {
        _unique();
        _data->scale(x);
        return *this;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Exchanges pointers to SharedArrays.
    // Useful for preventing unneccessary destructor calls.
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::swap(Matrix &b)
    {
        std::swap(_data, b._data);      // all Matrix data must be swapped!
        std::swap(_rows, b._rows);
        std::swap(_cols, b._cols);
        std::swap(_t, b._t);
    }

    // Returns a pointer to the beginning of the array.
    inline Real* Matrix::begin() { _unique(); return _data->begin(); }
    // Returns a const pointer to the beginning of the array.
    inline const Real* Matrix::begin() const { return _data->begin(); }
    // Returns a pointer to the end of the array.
    inline Real* Matrix::end() { _unique(); return _data->end(); }
    // Returns a const pointer to the end of the array.
    inline const Real* Matrix::end() const { return _data->end(); }

    ////////////////////////////////////////////////////////////////////////////
    // Copies the vector data to the matrix
    ////////////////////////////////////////////////////////////////////////////
    inline void convert_vector_to_matrix(const Vector &v, Matrix &m)
    {
        SharedArray<Real>::delete_reference(m._data);
        m._data = v._data;
        m._cols = v.size();
        m._rows = 1;
        m._t = 0;
        m._data->add_reference();
    }
    ////////////////////////////////////////////////////////////////////////////
    // Copies the matrix data to the vector
    ////////////////////////////////////////////////////////////////////////////
    inline void convert_matrix_to_vector(const Matrix &m, Vector &v)
    {
        if (m._t) {
            Matrix &mt = const_cast<Matrix&>(m); // only rearranges memory
            mt._transpose_storage();
        }
        SharedArray<Real>::delete_reference(v._data);
        v._data = m._data;
        v._data->add_reference();
    }
    ////////////////////////////////////////////////////////////////////////////
    // Performs the actual transpose operation, (leaves _transpose as false)
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::_transpose_storage()
    {
        if (!_t) return;            // nothing to do
        Matrix T(rows(), cols());   // newly constructed T is not transpose
        FORij T(i,j) = MATRIX_DATA(i,j);
        *this = T;                  // _t will get overwritten here
    }
    ////////////////////////////////////////////////////////////////////////////
    // Makes sure matrix's data not shared, will transpose storage if _t is set
    ////////////////////////////////////////////////////////////////////////////
    inline void Matrix::_unique()
    {
        if (_data->reference_count()==1) return;
        if (_t) _transpose_storage();       // this will make matrix unique
        else _data = _data->unique();
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the inverse of a 3x3 matrix, does not check size.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix inv3x3(const Matrix &A, double *detA)
    {
        Matrix B(3,3);
        const double det_A = det(A);
        if (detA) *detA = det_A;
        const double invDetA = 1.0/det_A;
        B(0,0) = (A(1,1)*A(2,2)-A(1,2)*A(2,1))*invDetA;
        B(0,1) = (A(0,2)*A(2,1)-A(0,1)*A(2,2))*invDetA;
        B(0,2) = (A(0,1)*A(1,2)-A(0,2)*A(1,1))*invDetA;
        B(1,0) = (A(1,2)*A(2,0)-A(1,0)*A(2,2))*invDetA;
        B(1,1) = (A(0,0)*A(2,2)-A(0,2)*A(2,0))*invDetA;
        B(1,2) = (A(0,2)*A(1,0)-A(0,0)*A(1,2))*invDetA;
        B(2,0) = (A(1,0)*A(2,1)-A(1,1)*A(2,0))*invDetA;
        B(2,1) = (A(0,1)*A(2,0)-A(0,0)*A(2,1))*invDetA;
        B(2,2) = (A(0,0)*A(1,1)-A(0,1)*A(1,0))*invDetA;
        return B;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the inverse of a 2x2 matrix, does not check size.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix inv2x2(const Matrix &A, double *detA)
    {
        Matrix B(2,2);
        const double det_A = A(0,0)*A(1,1)-A(0,1)*A(1,0);
        if (detA) *detA = det_A;
        const double invDetA = 1.0/det_A;
        B(0,0)= A(1,1)*invDetA;    B(0,1)=-A(0,1)*invDetA;
        B(1,0)=-A(1,0)*invDetA;    B(1,1)= A(0,0)*invDetA;
        return B;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns of 1x1 matrix but you're doing something wrong if this is called.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix inv1x1(const Matrix &A, double *detA)
    {
        if (detA) *detA = A(0,0);
        Matrix B(1,1);
        B(0,0) = 1.0/A(0,0);
        return B;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Returns the a copy of the transpose of this matrix.
    ////////////////////////////////////////////////////////////////////////////
    inline Matrix tr(const Matrix &A)
    {
        Matrix At(A);
        At.t();  // don't return reference or extra copy constructor gets called
        return At;
    }
    // returns the sum of matrices A and B
    inline Matrix operator+(const Matrix &A, const Matrix &B)
    {
        Matrix C(A);
        C+=B;
        return C;
    }
    // returns the difference of matrices A and B
    inline Matrix operator-(const Matrix &A, const Matrix &B)
    {
        Matrix C(A);
        C-=B;
        return C;
    }
}


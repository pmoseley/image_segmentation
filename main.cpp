////////////////////////////////////////////////////////////////////////////////////////////////
// Author: Philip Moseley
//         philip.moseley@u.northwestern.edu
////////////////////////////////////////////////////////////////////////////////////////////////
#include <vector>
#include <cmath>
#include <omp.h>
#include <mkl.h>
#include <istream>
#include "utility/Region.h"
#include "levelset/LevelSet_PDE.h"
#include "io/CSVHelper.h"

#define PI 3.1415927

using namespace matcad;

////////////////////////////////////////////////////////////////////////////////////////////////
//! Create the initial signed distance function.
////////////////////////////////////////////////////////////////////////////////////////////////
Vector create_init_sdf(Matrix &grid) {
    // This creates a square levelset %border away from the edges of the domain.
    double percent_border = 0.15;
    Vector phi(grid.rows()), row, min, max;
    Region bounds = find_bounds(grid);
    min = bounds.get_min();
    max = bounds.get_max();
    for(unsigned i=0; i<bounds.num_dim(); i++){
        min(i) += percent_border * bounds.range(i);
        max(i) -= percent_border * bounds.range(i);
    }
    bounds.set_min(min);
    bounds.set_max(max);
    for(unsigned i=0; i<grid.rows(); i++){
        if(bounds.point_in_region(grid.row(i))) phi(i) = -6.0;
        else phi(i) = 6.0;
    }
    return phi;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
int main(int narg, char **argv){
    if(narg!=2){
        LOG<<"No input file specified. Please give a *.csv file with the edge_indicator data.\n";
        return -1;
    }
    mkl_set_num_threads(2);
    const string img_file(argv[1]);
    LOG<<"Reading "<<img_file<<"...\n";

    //*********************USER CONFIG*************************//
    bool implicit = true;
    unsigned steps = 250;
    unsigned num_prints = 10;
    // ls.segment_image(g, bounds, false, 100, 5.0, 7.0, 6.00, 0.040);
    // double lambda(5.0), nu(9.00), dt(50.00), mu(0.05);
    double lambda(5.0), nu(3.00), dt(40.00), mu(0.01);
    //*********************************************************//
    unsigned steps_per_print(steps/num_prints);

    // Read in the edge indicator function.
    EdgeIndicator ei = read_csv(img_file);
    std::cout<<"Running, "<<ei.g.size()<<" points, "<<sqrt(ei.g.size())<<" square.\n";
    write_csv("grid",ei.grid);

    // Calculate the level set function.
    LevelSet_PDE ls(ei.grid, ei.conn);
    Vector custom_sdf = create_init_sdf(ei.grid);
    ls.set_phi(custom_sdf);
    write_csv("levelset0",ls.phi());
    // Segment the image.
    for(unsigned i=1; i<=num_prints; i++){
        ls.segment_image(ei.g, ei.bounds, implicit, steps_per_print, lambda, nu, dt, mu);
        write_csv("levelset"+utility::to_string(i),ls.phi());
        LOG<<"Step "<< i*steps_per_print <<" written to disk.\n";
    }
    LOG<<"Completed calculations.\n";
    return 0;
}

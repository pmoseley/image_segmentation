#pragma once
#include "SharedArray.h"

// if not transpose _t = 0 -> i + j*rows()
// if     transpose _t = 1 -> j + i*ncols()
#define MATRIX_DATA(i,j) (*_data)[(!_t)*((i)+(j)*_rows) + (_t)*((j)+(i)*_rows) ]
#define FORji for(unsigned j=0; j<cols(); j++) for(unsigned i=0; i<rows(); i++)
#define FORij for(unsigned i=0; i<rows(); i++) for(unsigned j=0; j<cols(); j++)

namespace matcad
{
    class Vector;    // Forward declaration of a Vector class.
    //! Basic Matrix class.
    class Matrix {
        //! Matrix friend functions
        //@{
        //! returns the matrix vector product: A*x
        friend Vector operator*(const Matrix &A, const Vector &x);
        //! returns the tranpose-matrix vector product: A'x
        friend Vector operator*(const Vector &x, const Matrix &A);
        //! returns the matrix-matrix product: A*B
        friend Matrix operator*(const Matrix &A, const Matrix &B);
        //! outputs the Matrix to a stream
        friend std::ostream& operator<<(std::ostream& os, const Matrix& A);
        //! Allows Vectors to be converted directly to matrices.
        friend void convert_vector_to_matrix(const Vector &v, Matrix &m);
        //! Allows matrices to be converted directly to vectors.
        friend void convert_matrix_to_vector(const Matrix &m, Vector &v);
        //! allows operator *= to access Vector private data
        template<class T> friend T& operator*=(T &x, const Real &s);
        //! returns the maximum absolute value of a vector or matrix
        template<class T> friend Real max_abs(const T &a);
        //! returns the minimum absolute value of a vector or matrix
        template<class T> friend Real min_abs(const T &a);
        //! returns the maximum value of a vector or matrix
        template<class T> friend Real max(const T &a);
        //! returns the minimum value of a vector or matrix
        template<class T> friend Real min(const T &a);
        //@}

        //! performs the actual transpose operation, (leaves _t as false)
        void _transpose_storage();
        //! makes sure this matrix's data is not shared
        void _unique();

        unsigned _t;     // ! flag for whether or not matrix is transposed (can be 0 or 1)
        SharedArray<Real> *_data;
        unsigned _rows, _cols;
    public:
        //! constructor - (defaults: empty Matrix, do not prezero elements)
        Matrix(unsigned nrow=0, unsigned ncol=0, bool zero=false);
        //! copy constructor from another Matrix
        Matrix(const Matrix &c);
        //! copy constructor from Vector, defaults to a column matrix
        explicit Matrix(const Vector &c, unsigned ncol=1);
        //! Copy constructor that copies from iterators.
        Matrix(const std::vector<Real> &x, unsigned m=~0, unsigned n=1);
        //! destructor
        ~Matrix();

        //! changes the length of the vector
        Matrix& resize(unsigned nrow, unsigned ncol, bool copy=false);
        //! Toggles the transpose flag on this Matrix.
        Matrix& t();
        //! Returns the number of rows in the Matrix.
        unsigned rows() const;
        //! Returns the number of columns in the Matrix.
        unsigned cols() const;
        //! Returns the number of elements in the Matrix.
        unsigned size() const;
        //! Returns true if the Matrix has no elements.
        bool empty() const;
        //! Returns true if the Matrix is square.
        bool square() const;
        //! Checks the Matrix size
        bool is_size(unsigned nrow, unsigned ncol) const;

        //! row and column operations
        //{
        //! Sets the difference between two rows.
        void row_difference(unsigned a, unsigned b, Vector &rab) const;
        //! returns a row of the matrix
        Vector row(const unsigned i) const;
        //! returns a column of the matrix
        Vector column(const unsigned j) const;
        //! sets the matrix row to the vector row
        void set_row(const unsigned i, const Vector &row);
        //! sets the matrix column to the vector col
        void set_column(const unsigned j, const Vector &col);
        //! Scales each row by the corresponding element in input Vector.
        void scale_rows(const Vector &s);
        //! Scales each column by the corresponding element in input Vector.
        void scale_columns(const Vector &s);
        //! adds a scaled vector to a row
        void add_to_row(const unsigned i, const Vector &x, double alpha=1.0);
        //! adds a scaled vector to a column
        void add_to_column(const unsigned j, const Vector &x, double alpha=1.0);
        //}

        //! assigns all elements to zero
        Matrix& zero();
        //! resizes and assigns all elements to zero
        Matrix& zero(unsigned nrow, unsigned ncol);
        //! assignment operator
        Matrix &operator=(const Matrix &c);
        //! returns a copy of the vector element, i
        Real  operator()(unsigned i, unsigned j) const;
        //! returns a reference to the vector element, i
        Real& operator()(unsigned i, unsigned j);
        //! Extracts matrix subset, empty argument returns all indices in the dimension.
        Matrix operator()(const vectorU32 &I, const vectorU32 &J) const;
        //! adds a matrix to this one
        Matrix& operator+=(const Matrix &r);
        //! subtracts a matrix from this one
        Matrix& operator-=(const Matrix &r);
        //! Adds a scaled matrix to this one, default scaling factor is unity.
        Matrix& add_scaled(const Matrix &r, const double s=1.0);
        //! Adds a subset of a scaled matrix to this one, default scaling factor is unity.
        Matrix& add_scaled(const vectorU32 &I, const Matrix &r, const double s=1.0);
        //! scales a matrix by a constant and returns a reference to itself
        Matrix& operator*=(const Real &x);
        //! Swaps data with another Matrix
        void swap(Matrix &b);

        //! Returns iterators (pointers) to the beginning and end of the matrix.
        //@{
        //! Returns a pointer to the beginning of the array.
        Real* begin();
        //! Returns a const pointer to the beginning of the array.
        const Real* begin() const;
        //! Returns a pointer to the end of the array.
        Real* end();
        //! Returns a const pointer to the end of the array.
        const Real* end() const;
        //@}

        //! outputs the vector as a string
        string to_string(const string &delimiter="\t", const string &eol="") const;
    };
    //! Returns a matrix with random values between (0,1).
    Matrix rand(unsigned rows, unsigned cols);
    //! returns the determinant of a square matrix
    Real det(const Matrix &A);
    //! Returns inverse of a matrix and optionally returns the determinant.
    Matrix inv(const Matrix &A, double *detA=NULL);
    //! Computes the eigenvalues of the real matrix.
    Vector eig(const Matrix &A);
    //! Returns the inverse of a 3x3 matrix, does not check size.
    Matrix inv3x3(const Matrix &A, double *detA);
    //! Returns the inverse of a 2x2 matrix, does not check size.
    Matrix inv2x2(const Matrix &A, double *detA);
    //! Returns the transpose of this matrix.
    Matrix tr(const Matrix &A);
}
#include "Matrix-inl.h"
#include "Vector-Matrix-inl.h"
#undef FORij
#undef FORji
